import React from 'react';
import { useCallback, useEffect } from 'react';

import { useAppSelector, useAppDispatch } from '../../src/store/hooks';
import { StyleSheet, TouchableOpacity } from 'react-native';
import { AppStyles } from '../../src/AppStyles';
import { Text, View } from '../../src/components/Themed';
import { add, clear, selectNextQuestionForPack } from './questionSlice';
import realm from '../../src/database/realm';
import { loadFromStorage, answerQuestion } from './questionSlice';

export function Questions({ navigation, packId }) {
    const question = useAppSelector(selectNextQuestionForPack(packId));
    const dispatch = useAppDispatch();

    const buttonStyles = [styles.flItem, styles.verticalCenter];

    const chooseOption1 = useCallback(() => {
        dispatch(answerQuestion({ id: question.id, answer: false }));
    }, []);

    const chooseOption2 = useCallback(() => {
        dispatch(answerQuestion({ id: question.id, answer: true }));
    }, []);

    return (
        <>
            {question && (
                <>
                    <Text>{JSON.stringify(question.option_1)}</Text>
                    <Text>{JSON.stringify(question.option_2)}</Text>
                    <Text>{JSON.stringify(question.answer)}</Text>
                    <TouchableOpacity onPress={chooseOption1} style={buttonStyles}>
                        <Text>{'Option 1'}</Text>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={chooseOption2} style={buttonStyles}>
                        <Text>{'Option 2'}</Text>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={chooseOption1} style={buttonStyles}>
                        <Text>{'Add to Fav'}</Text>
                    </TouchableOpacity>
                </>
            )}
        </>
    );
}

const styles = StyleSheet.create({
    flItem: {
        width: '100%',
        alignSelf: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    verticalCenter: {
        marginTop: 'auto',
        marginBottom: 'auto'
    },
    item: {
        padding: 13,
        marginVertical: 0,
        flexDirection: 'column',
        justifyContent: 'space-between',
        backgroundColor: AppStyles.color.white
    },
    item_title: {
        fontSize: 16,
        color: '#1E3163'
    }
});
