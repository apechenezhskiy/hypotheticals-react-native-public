import { createSlice, PayloadAction, createSelector } from '@reduxjs/toolkit';
import { RootState } from '../../src/store/index';
import { getQuestions, updateQuestions } from '../../src/database/QuestionStore';
import { selectPackById, selectPacks } from '../packs/packsSlice';

export interface QuestionState {
    id: string;
    option_1: string;
    option_2: string;
    answer?: boolean;
}

type QuestionsState = Array<QuestionState>;

const initialState: QuestionsState = [];

export const questionsSlice = createSlice({
    name: 'questions',
    initialState,
    reducers: {
        add: (state, { payload }: PayloadAction<QuestionState>) => {
            state.push(payload);
        },
        remove: (state, { payload: index }: PayloadAction<number>) => {
            state.splice(index, 1);
        },
        clear: () => {
            return [];
        },
        edit: (state, { payload }: PayloadAction<QuestionState>) => {
            const existingPackIndex = state.findIndex((pack) => pack.id === payload.id);

            state[existingPackIndex] = { ...state[existingPackIndex], option_1: '111' };
        },
        loadFromStorage: () => {
            return getQuestions();
        },
        answerQuestion: (state, { payload }) => {
            updateQuestions(payload.id);
            const existingPackIndex = state.findIndex((pack) => pack.id === payload.id);
            console.log(getQuestions()[0].answer);
            console.log(state[0].answer);
        }
    }
});

export const { add, remove, clear, edit, loadFromStorage, answerQuestion } = questionsSlice.actions;

export const selectQuestions = (state: RootState): QuestionsState => state.questions;

export const selectNextQuestionForPack = (packId) => {
    return createSelector(selectPacks, (packs) => {
        const pack = packs.filter((item) => {
            return item.id === packId.toString();
        });
        return pack[0].questions[0];
    });
};

export const selectQuestionById = (state: RootState, id: string): QuestionState => {
    const question = state.questions.filter((item) => item.id === id);
    return question.length ? question[0] : undefined;
};

export default questionsSlice.reducer;
