import { createSlice, PayloadAction, createSelector } from '@reduxjs/toolkit';
import { RootState } from '../../src/store/index';
import { getQuestions } from '../../src/database/QuestionStore';
import { selectPacks } from '../packs/packsSlice';

export interface AnswerState {
    id: string;
    option_1: string;
    option_2: string;
}

type QuestionsState = Array<QuestionState>;

const initialState: QuestionsState = [];

export const answersSlice = createSlice({
    name: 'answers',
    initialState,
    reducers: {
        add: (state, { payload }: PayloadAction<QuestionState>) => {
            state.push(payload);
        },
        remove: (state, { payload: index }: PayloadAction<number>) => {
            state.splice(index, 1);
        },
        clear: () => {
            return [];
        },
        edit: (state, { payload }: PayloadAction<QuestionState>) => {
            const existingPack = state.find((pack) => pack.id === payload.id);
            if (existingPack) {
                existingPack.option_1 = payload.option_1;
                existingPack.option_2 = payload.option_2;
            }
        },
        loadFromStorage: () => {
            return getQuestions();
        }
    }
});

export const { add, remove, clear, edit, loadFromStorage } = questionsSlice.actions;

export const selectQuestions = (state: RootState): QuestionsState => state.questions;

export const selectNextQuestionForPack = (packId) => {
    return createSelector(selectPacks, (packs) => {
        const pack = packs.filter((item) => {
            return item.id === packId.toString();
        });
        return pack[0].questions[0];
    });
};

export default questionsSlice.reducer;
