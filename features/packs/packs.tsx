import React from 'react';
import { useCallback } from 'react';

import { useAppSelector, useAppDispatch } from '../../src/store/hooks';
import { StyleSheet, TouchableOpacity } from 'react-native';
import { AppStyles } from '../../src/AppStyles';
import { Text, View } from '../../src/components/Themed';
import { FlatList } from '../../src/components/FlatList';
import { FlatListItem } from '../../src/components/FlatListItem';
import { add, clear, selectPacks, selectPackById } from './packsSlice';
import { useEffect } from 'react';
import realm from '../../src/database/realm';
import { loadFromStorage } from './packsSlice';
import { loadFromStorage as loadQuestions } from '../question/questionSlice';
import 'hypotheticals-pure//src/locales/index';
import {useTranslation} from 'react-i18next';

export function Packs({ navigation }) {
    const {t} = useTranslation();
    //const packs = useAppSelector((state) => [selectPackById(state, '1')]);
    const packs = useAppSelector(selectPacks);
    const dispatch = useAppDispatch();

    useEffect(() => {
        dispatch(loadFromStorage());
    }, []);

    const renderItem = useCallback((item) => {
        const goToPack = () => {
            navigation.navigate('Pack', { packId: item.id });
        };

        return (
            <TouchableOpacity onPress={goToPack} style={[styles.flItem, styles.verticalCenter]}>
                <View style={styles.item}>
                    <Text style={styles.item_title}>{item?.name + ' (' + item?.questions.length + ')'}</Text>
                </View>
            </TouchableOpacity>
        );
    }, []);

    return (
        <>
            <FlatList headerText={t('home.choosePack')}>
                {packs &&
                    packs.length > 0 &&
                    packs.map((pack, i) => {
                        return (
                            <FlatListItem key={i} isLastItem={i === packs.length - 1}>
                                {renderItem(pack)}
                            </FlatListItem>
                        );
                    })}
            </FlatList>
        </>
    );
}

const styles = StyleSheet.create({
    flItem: {
        width: '100%',
        alignSelf: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    verticalCenter: {
        marginTop: 'auto',
        marginBottom: 'auto'
    },
    item: {
        padding: 13,
        marginVertical: 0,
        flexDirection: 'column',
        justifyContent: 'space-between',
        backgroundColor: AppStyles.color.white
    },
    item_title: {
        fontSize: 16,
        color: '#1E3163'
    }
});
