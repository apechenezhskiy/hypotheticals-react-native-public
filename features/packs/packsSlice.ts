import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '../../src/store/index';
import { getPacks } from '../../src/database/PackStore';
import { QuestionState } from '../question/questionSlice';

export interface PackState {
    id: string;
    name: string;
    description: string;
    color?: string;
    size: number;
    made_choice_count?: number;
    questions: QuestionState[];
}

type PacksState = Array<PackState>;

const initialState: PacksState = [];

export const packsSlice = createSlice({
    name: 'packs',
    initialState,
    reducers: {
        add: (state, { payload }: PayloadAction<PackState>) => {
            state.push(payload);
        },
        remove: (state, { payload: index }: PayloadAction<number>) => {
            state.splice(index, 1);
        },
        clear: () => {
            return [];
        },
        edit: (state, { payload }: PayloadAction<PackState>) => {
            const existingPack = state.find((pack) => pack.id === payload.id);
            if (existingPack) {
                existingPack.name = payload.name;
            }
        },
        loadFromStorage: () => {
            return getPacks();
        }
    }
});

export const { add, remove, clear, edit, loadFromStorage } = packsSlice.actions;

export const selectPacks = (state: RootState): PacksState => {
    return state.packs;
};

export const selectPackById = (state: RootState, id: string): PackState => {
    const pack = state.packs.filter((item) => item.id === id);
    return pack.length ? pack[0] : undefined;
};

export default packsSlice.reducer;
