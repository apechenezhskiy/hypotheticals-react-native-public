import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    headerBackgroundScreen: {
        position: 'relative',
        backgroundColor: '#FFFFFF08',
        borderBottomLeftRadius: 50,
        borderBottomRightRadius: 50,
        height: 500,
        width: '100%'
    },
    anotherScreen: {
        top: -150,
        backgroundColor: 'transparent'
    },
    flatListColumnWrapperStyle: {
        justifyContent: 'space-between',
        marginBottom: 15,
        marginTop: 20
    },
    flatListItem: {
        width: '45%',
        height: 150,
        padding: 8,
        borderRadius: 10
    },
    flatListItemOpacity: {
        flex: 1,
        backgroundColor: 'transparent'
    },
    flatListItemOpacityPaid: {
        opacity: 0.3
    },
    verticalCenter: {
        marginTop: 'auto',
        marginBottom: 'auto'
    },
    flatListInnerItem: {
        padding: 13,
        marginVertical: 0,
        flexDirection: 'column',
        justifyContent: 'space-between',
        backgroundColor: '#C82D8B',
        alignItems: 'center',
        alignContent: 'stretch',
        height: '100%',
        borderRadius: 10,
        top: -70
    },
    flatListInnerItemPaid: {
        top: -110,
        height: 134
    },
    item_title: {
        fontSize: 20,
        marginTop: 20,
        color: '#FFFFFF'
    },
    item_description: {
        fontSize: 12,
        color: '#FFFFFF',
        textAlign: 'center'
    },
    item_count: {
        fontSize: 20,
        color: '#FFFFFF'
    },
    item_icon: {
        backgroundColor: 'transparent',
        zIndex: 2,
        width: 70,
        height: 70,
        top: -30,
        marginLeft: 'auto',
        marginRight: 'auto'
    },
    item_icon_paid: {
        top: -70
    },
    labelText: {
        fontSize: 17,
        marginLeft: 'auto',
        marginRight: 'auto',
        marginTop: 10,
        marginBottom: 10,
        color: '#FFFFFF'
    },
    locked_icon: {
        backgroundColor: 'transparent',
        zIndex: 99999,
        marginLeft: 'auto',
        marginRight: 'auto',
        top: 50,
        left: 5
    },
    headerRightIcon: {
        paddingRight: 10,
        paddingTop: 10,
        backgroundColor: 'transparent'
    },
    languageListItem: {
        width: '45%',
        padding: 8,
        borderRadius: 10
    },
    languageListItemIcon: {
        backgroundColor: 'transparent',
        zIndex: 2,
        marginLeft: 'auto',
        marginRight: 'auto'
    },
    languageTitle: {
        fontSize: 14,
        marginLeft: 'auto',
        marginRight: 'auto',
        color: '#FFFFFF'
    },
    languageSelectedTitle: {
        fontWeight: '600'
    },
    languageSettingsHeader: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    languageSettingsHeaderText: {
        fontSize: 18,
        marginTop: 10,
        color: '#FFFFFF'
    },
    languageSettingsChildren: {
        backgroundColor: '#475490',
        alignItems: 'center',
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        paddingBottom: 20
    },
    languageSettingsListColumnWrapperStyle: {
        justifyContent: 'space-between'
    }
});
