import React, { useState, useEffect, useCallback, useMemo, useRef } from 'react';
import { View, Text } from 'hypotheticals-pure/src/components/Themed';
import { useIsFocused } from '@react-navigation/native';
import { ActivityIndicator, TouchableOpacity, FlatList, Alert } from 'react-native';
import { getPacks } from 'hypotheticals-pure/src/database/PackStore';
import AmplitudeUtils from 'hypotheticals-pure/src/services/amplitudeUtils';
import LinearGradient from 'react-native-linear-gradient';
import { SvgXml } from 'react-native-svg';
import styles from './Home.style';
import { Pack } from 'hypotheticals-pure/src/database/objects/Pack';
import LockedSVG from 'hypotheticals-pure/src/components/svgs/LockedSVG';
import WrenchSVG from 'hypotheticals-pure/src/components/svgs/WrenchSVG';
import RussiaSVG from 'hypotheticals-pure/src/components/svgs/flags/RussiaSVG';
import USSVG from 'hypotheticals-pure/src/components/svgs/flags/USSVG';
import FranceSVG from 'hypotheticals-pure/src/components/svgs/flags/FranceSVG';
import GermanySVG from 'hypotheticals-pure/src/components/svgs/flags/GermanySVG';
import ItalySVG from 'hypotheticals-pure/src/components/svgs/flags/ItalySVG';
import SpainSVG from 'hypotheticals-pure/src/components/svgs/flags/SpainSVG';
import PortugalSVG from 'hypotheticals-pure/src/components/svgs/flags/PortugalSVG';
import JapanSVG from 'hypotheticals-pure/src/components/svgs/flags/JapanSVG';
import { Modalize } from 'react-native-modalize';
import * as Keychain from 'react-native-keychain';

import { StackScreenProps } from '@react-navigation/stack';
import { HomeTabParamList, LanguageSettingsType } from 'hypotheticals-pure/src/types';
import 'hypotheticals-pure//src/locales/index';
import {useTranslation} from 'react-i18next';
import {CHOSEN_LANGUAGE} from '../services/api';

export default function Home({ navigation }: StackScreenProps<HomeTabParamList, 'Home'>) {
    const {t, i18n} = useTranslation();
    const [packs, setPacks] = useState<Pack[]>([]);
    const isFocused = useIsFocused();
    const modalizeRef = useRef<Modalize>(null);
    const [lang, setLang] = useState<string>(i18n.language);

    useEffect(() => {
        setLang(i18n.language);
    }, [i18n.language])

    const getAnsweredCount = useCallback((pack: Pack): number => {
        return pack?.questions?.filter((question) => question.answer !== null).length;
    }, []);

    const getFavouriteCounts = useCallback(() => {
        if (packs[packs.length - 1]?.id === '-1') {
            return packs[packs.length - 1]?.questions?.length;
        }
        return 0;
    }, [packs]);

    useEffect(() => {
        setPacks(getPacks(false));
        let answeredCounts = 0;
        for (let i = 0; i < packs.length; i++) {
            answeredCounts += getAnsweredCount(packs[i]);
        }
        AmplitudeUtils.setUserProperties({ answeredCounts: answeredCounts, favouriteCounts: getFavouriteCounts() });
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [isFocused]);

    const getPackQuestionInfo = useCallback(
        (item: Pack) => {
            return ` ( ${getAnsweredCount(item)} / ${item?.questions?.length} )`;
        },
        [getAnsweredCount]
    );

    const getGradientColors = useCallback((itemColorFrom: string, itemColorTo: string) => {
        return [itemColorFrom || '#C82D8B', itemColorTo || '#C82181'];
    }, []);

    const renderItem = useCallback(
        ({ item }: { item: Pack }) => {
            const goToPack = () => {
                AmplitudeUtils.logEventAmpl('openPack', { itemId: item.id });
                navigation.navigate('Pack', { packId: item.id });
            };

            const xml = `
            <svg height="70" width="70" viewBox="0 0 48 48" xmlns="http://www.w3.org/2000/svg">
            <path d="M46.52,11.29a1.89,1.89,0,0,0-2.77,1.38,8.54,8.54,0,0,1-4.35,6.21c0,.35,0,.7,0,1.06,0,3.22.13,5.92.35,8.18l1.79-1.87A21.56,21.56,0,0,0,47.49,13.1,1.9,1.9,0,0,0,46.52,11.29Z" fill="#f0f0f0" stroke="#45413c" strokeLinecap="round" strokeLinejoin="round"/>
            <path d="M8.21,28.12c.22-2.26.35-5,.35-8.18,0-.36,0-.71,0-1.06a8.54,8.54,0,0,1-4.35-6.21,1.88,1.88,0,0,0-3.74.43A21.56,21.56,0,0,0,6.42,26.25Z" fill="#f0f0f0" stroke="#45413c" strokeLinecap="round" strokeLinejoin="round"/>
            <path d="M39.44,19.94a15.44,15.44,0,0,0-30.88,0c0,18.14-4.28,19.68-2.7,19.68,3.78,0,4.63-1.93,7-1.93s3.09,1.93,5.79,1.93,3.08-1.93,5.4-1.93,2.7,1.93,5.4,1.93,3.48-1.93,5.79-1.93,3.17,1.93,7,1.93C43.72,39.62,39.44,38.08,39.44,19.94Z" fill="#f0f0f0"/>
            <path d="M35.91,35.46c-2.46,0-3.28,1.9-6.16,1.9s-3.29-1.9-5.75-1.9-2.88,1.9-5.75,1.9-3.7-1.9-6.16-1.9C10,35.46,9,36.79,6.4,37.22c-.76,2.07-1.26,2.4-.54,2.4,3.78,0,4.63-1.93,7-1.93s3.09,1.93,5.79,1.93,3.08-1.93,5.4-1.93,2.7,1.93,5.4,1.93,3.48-1.93,5.79-1.93,3.17,1.93,7,1.93c.72,0,.22-.33-.54-2.4C39,36.79,38,35.46,35.91,35.46Z" fill="#e0e0e0"/>
            <path d="M19.370 7.590 A4.63 1.16 0 1 0 28.630 7.590 A4.63 1.16 0 1 0 19.370 7.590 Z" fill="#ffffff"/>
            <path d="M6.500 44.500 A17.5 1.5 0 1 0 41.500 44.500 A17.5 1.5 0 1 0 6.500 44.500 Z" fill="#45413c" opacity=".15"/>
            <path d="M12.81,23.8c0,.64.86,1.15,1.93,1.15s1.93-.51,1.93-1.15-.87-1.16-1.93-1.16S12.81,23.16,12.81,23.8Z" fill="#ffaa54"/>
            <path d="M12.81,23.8c0,.64.86,1.15,1.93,1.15s1.93-.51,1.93-1.15-.87-1.16-1.93-1.16S12.81,23.16,12.81,23.8Z" fill="#e0e0e0"/>
            <path d="M35.19,23.8c0,.64-.86,1.15-1.93,1.15s-1.93-.51-1.93-1.15.87-1.16,1.93-1.16S35.19,23.16,35.19,23.8Z" fill="#e0e0e0"/>
            <path d="M33.26,18.39a2.7,2.7,0,1,1-2.7-2.7A2.7,2.7,0,0,1,33.26,18.39Z" fill="#656769"/>
            <path d="M30.56,17.24a2.7,2.7,0,0,1,2.58,1.92,2.76,2.76,0,0,0,.12-.77,2.7,2.7,0,1,0-5.4,0,2.76,2.76,0,0,0,.12.77A2.7,2.7,0,0,1,30.56,17.24Z" fill="#525252"/>
            <path d="M33.26,18.39a2.7,2.7,0,1,1-2.7-2.7A2.7,2.7,0,0,1,33.26,18.39Z" fill="none" stroke="#45413c" strokeLinecap="round" strokeLinejoin="round"/>
            <path d="M13.58,17.24a3.86,3.86,0,1,0,3.86-3.86A3.85,3.85,0,0,0,13.58,17.24Z" fill="#656769"/>
            <path d="M17.44,15.31a3.84,3.84,0,0,1,3.72,2.89,3.86,3.86,0,1,0-7.44,0A3.84,3.84,0,0,1,17.44,15.31Z" fill="#525252"/>
            <path d="M13.58,17.24a3.86,3.86,0,1,0,3.86-3.86A3.85,3.85,0,0,0,13.58,17.24Z" fill="none" stroke="#45413c" strokeLinecap="round" strokeLinejoin="round"/>
            <path d="M39.44,19.94a15.44,15.44,0,0,0-30.88,0c0,18.14-4.28,19.68-2.7,19.68,3.78,0,4.63-1.93,7-1.93s3.09,1.93,5.79,1.93,3.08-1.93,5.4-1.93,2.7,1.93,5.4,1.93,3.48-1.93,5.79-1.93,3.17,1.93,7,1.93C43.72,39.62,39.44,38.08,39.44,19.94Z" fill="none" stroke="#45413c" strokeLinecap="round" strokeLinejoin="round"/>
            <path d="M18.19,25.67c.38,6.21,3.93,8.47,5.28,9.24a1.11,1.11,0,0,0,1.06,0c1.35-.77,4.9-3,5.28-9.24a1.06,1.06,0,0,0-1.06-1.12h-9.5A1.06,1.06,0,0,0,18.19,25.67Z" fill="#ffb0ca" stroke="#45413c" strokeLinecap="round" strokeLinejoin="round"/>
            <path d="M24,29.58c0-1.15,3.47-.81,3.47.39v5a3.47,3.47,0,0,1-6.94,0V30C20.53,28.77,24,28.43,24,29.58Z" fill="#ff6242"/>
            <path d="M24,29.58c0-1.15-3.47-.81-3.47.39v1.54c0-1.2,6.94-1.2,6.94,0V30C27.47,28.77,24,28.43,24,29.58Z" fill="#ff866e"/>
            <path d="M24,29.58c0-1.15,3.47-.81,3.47.39v5a3.47,3.47,0,0,1-6.94,0V30C20.53,28.77,24,28.43,24,29.58Z" fill="none" stroke="#45413c" strokeLinecap="round" strokeLinejoin="round"/>
        </svg>`;

            const isPaid = false; //item.isPaid;
            return (
                <TouchableOpacity activeOpacity={1} onPress={goToPack} style={[styles.flatListItem, styles.verticalCenter]}>
                    <View style={styles.locked_icon}>{isPaid && <LockedSVG />}</View>
                    <View style={[styles.flatListItemOpacity, isPaid ? styles.flatListItemOpacityPaid : undefined]}>
                        <View style={[styles.item_icon, isPaid ? styles.item_icon_paid : undefined]}>
                            <SvgXml xml={item.svg_image || xml} width={'100%'} height={'100%'} />
                        </View>
                        <LinearGradient
                            colors={getGradientColors(item.color.colorFrom, item.color.colorTo)}
                            style={[styles.flatListInnerItem, isPaid ? styles.flatListInnerItemPaid : undefined]}
                        >
                            <Text style={styles.item_title}>{getPackLangText(item, 'name')}</Text>
                            <Text style={styles.item_description}>{getPackLangText(item, 'description')}</Text>
                            <Text style={styles.item_count}>{getPackQuestionInfo(item)}</Text>
                        </LinearGradient>
                    </View>
                </TouchableOpacity>
            );
        },
        [getPackQuestionInfo, navigation, getGradientColors, lang]
    );

    const getPackLangText = useCallback((pack: Pack, property: string)=> {
        if (pack?.textsJson && pack?.textsJson[lang]) {
            const texts = JSON.parse(pack?.textsJson[lang]);
            return texts[property] ||  pack[property]
        }

        return pack[property]
    }, [lang])

    const renderLanguageItem = useCallback(
        ({ item }: { item: LanguageSettingsType }) => {
            const handleLanguageChange = () => {
                Keychain.setInternetCredentials(CHOSEN_LANGUAGE, CHOSEN_LANGUAGE, item.locale)
                    .then(() => {
                        modalizeRef.current?.close();
                        return i18n.changeLanguage(item.locale)
                    })
                    .catch((err) =>  console.log(err));
            };

            const onChangeLanguageClick = () => {
                Alert.alert(t('common.confirm'), t('language.changeLanguage') + item.name.toLowerCase() + '?', [
                    {
                        text: t('common.cancel'),
                        style: 'cancel'
                    },
                    { text: t('common.ok'), onPress: handleLanguageChange }
                ]);
            };

            const isLang = item.locale === lang;
            return (
                <TouchableOpacity activeOpacity={1} onPress={onChangeLanguageClick} style={[styles.languageListItem, styles.verticalCenter]}>
                    <View style={[styles.languageListItemIcon]}>
                        <item.svg_image />
                    </View>
                    <Text style={[styles.languageTitle, isLang ? styles.languageSelectedTitle : undefined]}>
                        {item.name + (isLang ? ' ✅' : '')}
                    </Text>
                </TouchableOpacity>
            );
        },
        [lang]
    );

    const flatListHeader = useCallback(() => {
        return <Text style={styles.labelText}>{t('home.choosePack')}</Text>;
    }, [lang]);


    const languageSettingsListHeader = useCallback(() => {
        return <View style={styles.languageSettingsHeader}><Text style={styles.languageSettingsHeaderText}>{t('home.chooseLanguage')}</Text></View>;
    }, []);

    const packsListKeyExtractor = useCallback((item: Pack) => item.id.toString(), []);
    const languageSettingsListKeyExtractor = useCallback((item: LanguageSettingsType) => item.locale, []);

    const linearBackgroundColors = useMemo(() => ['#475490', '#202741'], []);

    React.useEffect(
        () =>
            navigation.addListener('beforeRemove', (e) => {
                // Prevent default behavior of leaving the screen
                e.preventDefault();
            }),
        [navigation]
    );

    const onSettingsClick = useCallback(() => {
        modalizeRef.current?.open();
    }, []);

    React.useLayoutEffect(() => {
        navigation.setOptions({
            headerRight: () => (
                <View style={styles.headerRightIcon}>
                    <TouchableOpacity onPress={onSettingsClick} accessible={true} accessibilityLabel={t('home.chooseLanguage')}>
                        <WrenchSVG />
                    </TouchableOpacity>
                </View>
            )
        });
    }, [onSettingsClick, navigation]);

    const getLanguageData = useCallback(()=> {
        // en, de, it, es, fr, pt, ja, ru
        // English, German, Italian, Spanish, French, Portuguese, Japanese, Russian
        return [
            {locale: 'en', name: t('language.english'), svg_image: USSVG},
            {locale: 'de', name: t('language.german'), svg_image: GermanySVG},
            {locale: 'it', name: t('language.italian'), svg_image: ItalySVG},
            {locale: 'es', name: t('language.spanish'), svg_image: SpainSVG},
            {locale: 'fr', name: t('language.french'), svg_image: FranceSVG},
            {locale: 'pt', name: t('language.portugese'), svg_image: PortugalSVG},
            {locale: 'ja', name: t('language.japanese'), svg_image: JapanSVG},
            {locale: 'ru', name: t('language.russian'), svg_image: RussiaSVG}
        ]
    }, [])

    return (
        <LinearGradient colors={linearBackgroundColors} style={styles.container}>
            {packs.length === 0 ? (
                <ActivityIndicator size={'large'} color={'#93A3B1'} />
            ) : (
                <>
                    <View style={styles.headerBackgroundScreen} />
                    <View style={styles.anotherScreen}>
                        <FlatList
                            columnWrapperStyle={styles.flatListColumnWrapperStyle}
                            data={packs}
                            numColumns={2}
                            renderItem={renderItem}
                            keyExtractor={packsListKeyExtractor}
                            ListHeaderComponent={flatListHeader}
                        />
                    </View>
                </>
            )}
            <Modalize
                ref={modalizeRef}
                flatListProps={{
                    data: getLanguageData(),
                    numColumns: 2,
                    renderItem: renderLanguageItem,
                    keyExtractor: languageSettingsListKeyExtractor,
                    showsVerticalScrollIndicator: false,
                    ListHeaderComponent: languageSettingsListHeader,
                    columnWrapperStyle: styles.languageSettingsListColumnWrapperStyle
                }}
                adjustToContentHeight
                childrenStyle={styles.languageSettingsChildren}
            />
        </LinearGradient>
    );
}
