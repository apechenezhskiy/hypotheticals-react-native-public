import React, { useState, useEffect, useCallback, useMemo, useRef } from 'react';
import { TouchableOpacity, Alert, StyleSheet, Platform } from 'react-native';
import * as Keychain from 'react-native-keychain';
import { View, Text } from 'hypotheticals-pure/src//components/Themed';
import styles from './Pack.style';
import {
    getQuestionByPackId,
    answerQuestions,
    changeFavStatus,
    getQuestionIndexInPack,
    getQuestionById,
    contentReport
} from 'hypotheticals-pure/src//database/QuestionStore';
import { getPackById } from 'hypotheticals-pure/src//database/PackStore';
import { Pack } from 'hypotheticals-pure/src//database/objects/Pack';
import { Question } from 'hypotheticals-pure/src//database/objects/Question';
import { useIsFocused } from '@react-navigation/native';
import AmplitudeUtils from 'hypotheticals-pure/src//services/amplitudeUtils';
import LinearGradient from 'react-native-linear-gradient';
import Api from 'hypotheticals-pure/src//services/api';
import { StackScreenProps } from '@react-navigation/stack';
import { PackTabParamList } from '../types';

import ProgressBar from 'hypotheticals-pure/src/components/ProgressBar';
import HeartSVG from 'hypotheticals-pure/src/components/svgs/HeartSVG';
import HeartEmptySVG from 'hypotheticals-pure/src/components/svgs/HeartEmptySVG';
import BackHandSVG from 'hypotheticals-pure/src/components/svgs/BackHandSVG';
import BackHandEmptySVG from 'hypotheticals-pure/src/components/svgs/BackHandEmptySVG';
import CheckBoxSVG from 'hypotheticals-pure/src/components/svgs/CheckBoxSVG';
import ExclamationMarkSVG from 'hypotheticals-pure/src/components/svgs/ExclamationMarkSVG';

import { Modalize } from 'react-native-modalize';

import {
    TourGuideZone, // Main wrapper of highlight component
    TourGuideZoneByPosition, // Component to use mask on overlay (ie, position absolute)
    useTourGuideController // hook to start, etc.
} from 'rn-tourguide';
import { requestPurchase, useIAP, Sku } from 'react-native-iap';

import 'hypotheticals-pure//src/locales/index';
import {useTranslation} from 'react-i18next';

const IS_TUTORIAL_COMPLETED = 'isTutorialCompleted';
const AVAILABLE_FREE_DILEMMAS_COUNT = 3;


const skus = Platform.select({
    ios: ['packsUnlock', 'testPurchse'],
    android: ['packsUnlock', 'testPurchse'],
});

export default function PackScreen({ navigation, route }: StackScreenProps<PackTabParamList, 'Pack'>) {
    const {t} = useTranslation();
    const { packId } = route.params;
    const [question, setQuestion] = useState<Question>();
    const [option1ChoosenBy, setoption1ChoosenBy] = useState(0);
    const [packInfo, setPackInfo] = useState<Pack>();
    const [isShowResult, setIsShowResult] = useState(false);
    const [isAnswerTop, setIsAnswerTop] = useState(true);
    const [, updateState] = React.useState();
    const forceUpdate = React.useCallback(() => updateState({}), []);
    const modalizeRef = useRef<Modalize>(null);

    const isFocused = useIsFocused();

    const {
        canStart, // a boolean indicate if you can start tour guide
        start, // a function to start the tourguide
        eventEmitter // an object for listening some events
    } = useTourGuideController();


    const {
        connected,
        products,
        getProducts,
    } = useIAP();

    useEffect(() => {
        if (connected) {
            try {
                getProducts({skus: skus || ['']});
            } catch (error) {
                console.error(`handleGetProducts: ${error}`);
            }
        }
    }, [connected]);

    const setTutorialCompleted = () => {
        Keychain.setInternetCredentials(IS_TUTORIAL_COMPLETED, IS_TUTORIAL_COMPLETED, 'true').catch(console.error);
    };

    const handleOnStop = useCallback(() => setTutorialCompleted(), []);

    useEffect(() => {
        eventEmitter?.on('stop', handleOnStop);

        return () => {
            eventEmitter?.off('stop', handleOnStop);
        };
    }, [eventEmitter, handleOnStop]);

    useEffect(() => {
        const startTutorial = async () => {
            const isTutorialCompleted = await Keychain.getInternetCredentials(IS_TUTORIAL_COMPLETED);
            if (!isTutorialCompleted) {
                start();
            }
        };

        if (canStart) {
            startTutorial().catch(console.error);
        }
    }, [canStart]);

    useEffect(() => {
        if (!isFocused) {
            if (question) {
                question.removeAllListeners();
            }
        }
    }, [isFocused, question]);

    const questionChanged = useCallback(
        (newQuestion: Question, changes) => {
            setQuestion(newQuestion);
            forceUpdate();
        },
        [forceUpdate]
    );

    const setQuestionByPackId = useCallback(() => {
        const questionByPackId: Question = getQuestionByPackId(packId, question?.id);
        setQuestion(questionByPackId);
        try {
            questionByPackId.addListener(questionChanged);
        } catch (error) {
            // eslint-disable-next-line @typescript-eslint/restrict-template-expressions
            console.error(`Unable to update the questions' state, an exception was thrown within the change listener: ${error}`);
        }
    }, [packId, question, questionChanged]);

    useEffect(() => {
        const initQuestion = () => {
            setQuestionByPackId();
        };
        initQuestion();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    useEffect(() => {
        setPackInfo(getPackById(packId));
        setoption1ChoosenBy(
            Math.round(((question?.option_1_stats || 1) / ((question?.option_1_stats || 0) + (question?.option_2_stats || 0))) * 100)
        );
    }, [question, packId]);

    const answerOption1 = useCallback(() => {
        if (question) {
            AmplitudeUtils.logEventAmpl('answerQuestion', {
                questionId: question.id,
                packId: packInfo?.id,
                value: true
            });
            Api.answersPost(question.id, true).catch(console.error);
            answerQuestions(question.id, true);
            setIsAnswerTop(true);
            setIsShowResult(true);
        }
    }, [question, packInfo]);

    const answerOption2 = useCallback(() => {
        if (question) {
            AmplitudeUtils.logEventAmpl('answerQuestion', {
                questionId: question.id,
                packId: packInfo?.id,
                value: false
            });
            Api.answersPost(question.id, false).catch(console.error);
            answerQuestions(question.id, false);
            setIsAnswerTop(false);
            setIsShowResult(true);
        }
    }, [question, packInfo]);

    const changeFavStatusPress = useCallback(() => {
        if (question) {
            AmplitudeUtils.logEventAmpl('changeFavQuestion', {
                questionId: question.id,
                packId: packInfo?.id,
                value: !question.isFavourite
            });
            changeFavStatus(question.id);
        }
    }, [question, packInfo]);

    const showNextQuestion = useCallback(() => {
        if (question) {
            const questionIndex = getQuestionIndexInPack(packId, question.id)
            console.log('questionIndex = ' + questionIndex);
            if (questionIndex > AVAILABLE_FREE_DILEMMAS_COUNT && products.length) {
                modalizeRef.current?.open();
            } else {
                AmplitudeUtils.logEventAmpl('showNextQuestion', {
                    questionId: question.id,
                    packId: packInfo?.id
                });
                question.removeAllListeners();
                setQuestionByPackId();
                setIsShowResult(false);
            }
        }
    }, [question, packInfo, setQuestionByPackId]);

    const linearBackgroundColors = useMemo(() => ['#475490', '#202741'], []);

    const capitalizeFirstLetter = useCallback((string: string) => {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }, []);

    const cardColorsTop = useMemo(() => ['#EA3966', '#F1A151'], []);
    const cardColorsBottom = useMemo(() => ['#3881F7', '#67A1FF'], []);
    const cardColorsTopOpacity = useMemo(() => ['#EA396660', '#F1A15160'], []);
    const cardColorsBottomOpacity = useMemo(() => ['#3881F760', '#67A1FF60'], []);
    const backgroundColorTransparent = useMemo(() => ({ backgroundColor: 'transparent' }), []);

    const onSubmitContentReportClick = useCallback(() => {
        if (question?.id) {
            Api.contentReportsPost(question.id).catch(console.error);
            AmplitudeUtils.logEventAmpl('contentReport', {
                questionId: question.id,
                packId: packInfo?.id
            });
            contentReport(question.id);
            question.removeAllListeners();
            setQuestionByPackId();
            setIsShowResult(false);
        }
    }, [question, packInfo, setQuestionByPackId]);

    const onContentReportClick = useCallback(() => {
        Alert.alert(t('common.report'), t('common.reportText'), [
            {
                text: t('common.cancel'),
                style: 'cancel'
            },
            { text: t('common.send'), onPress: onSubmitContentReportClick }
        ]);
    }, [onSubmitContentReportClick]);

    React.useLayoutEffect(() => {
        navigation.setOptions({
            headerRight: () => (
                <View style={styles.headerRightIcon}>
                    <TouchableOpacity onPress={onContentReportClick} accessible={true} accessibilityLabel={t('common.report')}>
                        <ExclamationMarkSVG />
                    </TouchableOpacity>
                </View>
            )
        });
    }, [onContentReportClick, navigation]);

    const handleClose = () => {
        if (modalizeRef.current) {
            modalizeRef.current.close();
        }
    };

    const handleBuyProduct = async (sku: Sku) => {
        try {
            await requestPurchase({sku});
        } catch (error) {
            console.error(`handleBuyProduct: ${error}`);
        }
    };

    const renderContent = () => (
        <View style={s.content}>
            {products.map(item => (
                <TouchableOpacity style={s.content__button} activeOpacity={0.75} onPress={()=> handleBuyProduct(item.productId)}>
                    <Text style={s.content__buttonText}>{(item.description + ' - ' + item.localizedPrice).toUpperCase()}</Text>
                </TouchableOpacity>
            ))}
        </View>)


    return (
        <LinearGradient colors={linearBackgroundColors} style={styles.container}>
            {question && packInfo && (
                <>
                    <View style={styles.headerBackgroundScreen}>
                        <Text style={styles.packScreenTitle}>
                            {`${packInfo?.name}: ${getQuestionIndexInPack(packId, question.id) + 1} / ${packInfo.questions.length}`}
                        </Text>
                    </View>

                    <View style={styles.anotherScreen}>
                        <View style={styles.answersContainer}>
                            <TouchableOpacity
                                onPress={answerOption1}
                                disabled={isShowResult}
                                style={[styles.linearGradient, styles.answerCardContainerTop]}
                            >
                                <LinearGradient
                                    colors={isShowResult && !isAnswerTop ? cardColorsTopOpacity : cardColorsTop}
                                    style={[styles.linearGradient, styles.answerCardTop]}
                                >
                                    <View style={[styles.answerCardContainer, styles.answerCardTextTop]}>
                                        <CheckBoxSVG style={[!isShowResult || !isAnswerTop ? styles.invisible : null]} />
                                        <Text style={[styles.answerCardText, isShowResult && !isAnswerTop ? styles.answerCardTextOpacity : null]}>
                                            {capitalizeFirstLetter(question.option_1)}
                                        </Text>
                                    </View>
                                    {isShowResult && (
                                        <View style={styles.progressContainerTop}>
                                            <ProgressBar completed={option1ChoosenBy} isLeft={true} />
                                        </View>
                                    )}
                                </LinearGradient>
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={answerOption2}
                                disabled={isShowResult}
                                style={[styles.linearGradient, styles.answerCardContainerBottom]}
                            >
                                <LinearGradient
                                    colors={isShowResult && isAnswerTop ? cardColorsBottomOpacity : cardColorsBottom}
                                    style={[styles.linearGradient, styles.answerCardBottom]}
                                >
                                    {isShowResult && (
                                        <View style={styles.progressContainerBottom}>
                                            <ProgressBar completed={100 - option1ChoosenBy} isLeft={false} />
                                        </View>
                                    )}
                                    <View style={[styles.answerCardContainer, styles.answerCardTextBottom]}>
                                        <CheckBoxSVG style={[!isShowResult || isAnswerTop ? styles.invisible : null]} />
                                        <Text style={[styles.answerCardText, isShowResult && isAnswerTop ? styles.answerCardTextOpacity : null]}>
                                            {capitalizeFirstLetter(question.option_2)}
                                        </Text>
                                    </View>
                                </LinearGradient>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={styles.buttonsContainer}>
                        <TourGuideZone zone={3} shape={'circle'} text={t(`tutorial.step3`)} maskOffset={10}>
                            <TouchableOpacity
                                accessible={true}
                                accessibilityLabel={question.isFavourite ? 'Remove from favorite' : 'Add to favourites'}
                                onPress={changeFavStatusPress}
                            >
                                {question.isFavourite ? <HeartSVG /> : <HeartEmptySVG />}
                            </TouchableOpacity>
                        </TourGuideZone>
                        <TourGuideZone zone={2} shape={'circle'} text={t(`tutorial.step2`)} maskOffset={10}>
                            <TouchableOpacity accessible={isShowResult} accessibilityLabel={'Show next question'} onPress={showNextQuestion}>
                                {isShowResult ? <BackHandSVG /> : <BackHandEmptySVG />}
                            </TouchableOpacity>
                        </TourGuideZone>
                    </View>

                    <TourGuideZoneByPosition
                        zone={1}
                        text={t(`tutorial.step1`)}
                        shape={'circle'}
                        isTourGuide={true}
                        height={'40%'}
                        right={-1000}
                        containerStyle={backgroundColorTransparent}
                    />
                </>
            )}

            <Modalize
                ref={modalizeRef}
                adjustToContentHeight={true}
            >
                {renderContent()}
            </Modalize>
        </LinearGradient>
    );
}
const s = StyleSheet.create({
    content: {
      padding: 20,
    },

    content__icon: {
      width: 32,
      height: 32,

      marginBottom: 20,
    },

    content__subheading: {
      marginBottom: 2,

      fontSize: 16,
      fontWeight: '600',
      color: '#ccc',
    },

    content__heading: {
      fontSize: 24,
      fontWeight: '600',
      color: '#333',
    },

    content__description: {
      paddingTop: 10,
      paddingBottom: 10,

      fontSize: 15,
      fontWeight: '200',
      lineHeight: 22,
      color: '#666',
    },

    content__input: {
      paddingVertical: 15,
      marginBottom: 20,

      width: '100%',

      borderWidth: 1,
      borderColor: 'transparent',
      borderBottomColor: '#cdcdcd',
      borderRadius: 6,
    },

    content__button: {
      paddingVertical: 15,
      marginVertical: 20,

      width: '100%',

      backgroundColor: '#333',
      borderRadius: 6,
    },

    content__buttonText: {
      color: '#fff',
      fontSize: 15,
      fontWeight: '600',
      textAlign: 'center',
    },
  });
