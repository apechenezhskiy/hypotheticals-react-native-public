import * as React from 'react';
import { View } from 'hypotheticals-pure/src/components/Themed';
import { Text, Image, TouchableOpacity, StyleSheet } from 'react-native';
import { StackScreenProps } from '@react-navigation/stack';
import { OnboardingTabParamList } from 'hypotheticals-pure/src/types';

import Onboarding from 'react-native-onboarding-swiper';

export default function OnboardingScreen({ navigation }: StackScreenProps<OnboardingTabParamList, 'Onboarding'>) {
    const Dots = React.useCallback(({ selected }) => {
        const backgroundColor = selected ? 'rgba(0, 0, 0, 0.8)' : 'rgba(0, 0, 0, 0.3)';

        return (
            <View
                style={{
                    width: 6,
                    height: 6,
                    marginHorizontal: 3,
                    backgroundColor
                }}
            />
        );
    }, []);

    const Skip = React.useCallback(
        ({ ...props }) => (
            <>
                <TouchableOpacity style={{ marginHorizontal: 10 }} {...props}>
                    <Text style={{ fontSize: 16 }}>Skip</Text>
                </TouchableOpacity>
            </>
        ),
        []
    );

    const Next = React.useCallback(
        ({ ...props }) => (
            <TouchableOpacity style={{ marginHorizontal: 10 }} {...props}>
                <Text style={{ fontSize: 16 }}>Next</Text>
            </TouchableOpacity>
        ),
        []
    );

    const Done = React.useCallback(
        ({ ...props }) => (
            <TouchableOpacity style={{ marginHorizontal: 10 }} {...props}>
                <Text style={{ fontSize: 16 }}>Done</Text>
            </TouchableOpacity>
        ),
        []
    );

    const goHome = React.useCallback(() => navigation.navigate('Home'), [navigation]);

    return (
        <Onboarding
            SkipButtonComponent={Skip}
            NextButtonComponent={Next}
            DoneButtonComponent={Done}
            DotComponent={Dots}
            onSkip={goHome}
            onDone={goHome}
            pages={[
                {
                    backgroundColor: '#a6e4d0',
                    image: <Image source={require('hypotheticals-pure/assets/images/onboarding-img1.png')} />,
                    title: 'Игра для вечеринок с друзьями',
                    subtitle:
                        'Обсудите возмутительные гипотетические ситуации, моральные дилеммы, провокационные вопросы и примите сложнейшие решения.'
                },
                {
                    backgroundColor: '#fdeb93',
                    image: <Image source={require('hypotheticals-pure/assets/images/onboarding-img2.png')} />,
                    title: 'Выбери категорию вопросов',
                    subtitle: 'Уникальный и часто обновляемый контент'
                },
                {
                    backgroundColor: '#e9bcbe',
                    image: <Image source={require('hypotheticals-pure/assets/images/onboarding-img3.png')} />,
                    title: 'Сделай выбор',
                    subtitle: 'Прочитай громко возможные варианты, обсуди и сделай выбор'
                },
                {
                    backgroundColor: '#e9bcbe',
                    image: <Image source={require('hypotheticals-pure/assets/images/onboarding-img3.png')} />,
                    title: 'Статистика ответов',
                    subtitle: 'Узнай, что выбрали другие и переходи к следующей дилемме'
                }
            ]}
        />
    );
}
