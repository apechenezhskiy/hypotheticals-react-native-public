import * as React from 'react';
import { View } from 'hypotheticals-pure/src/components/Themed';

import { useCallback, useEffect } from 'react';
import { ActivityIndicator } from 'react-native';
import RNBootSplash from 'react-native-bootsplash';
import Api from 'hypotheticals-pure/src/services/api';
import { getPacks, upsertPacks, deletePacks } from 'hypotheticals-pure/src/database/PackStore';
import { showToaster } from 'hypotheticals-pure/src/helpers/showToaster';
import { StackScreenProps } from '@react-navigation/stack';
import { SplashTabParamList } from 'hypotheticals-pure/src/types';
import { Pack } from 'hypotheticals-pure/src/generated';
import * as Keychain from 'react-native-keychain';
import {useTranslation} from 'react-i18next';
import {CHOSEN_LANGUAGE, LOADED_PACKS_LANGUAGE} from '../services/api';

export default function Splash({ navigation }: StackScreenProps<SplashTabParamList, 'Splash'>) {
    const {i18n} = useTranslation();
    const splashScreenHide = useCallback(async () => {
        try {
            console.log("Splash screen useEffect " + i18n.language + ' === ');
            const chosenLanguageObject = await Keychain.getInternetCredentials(CHOSEN_LANGUAGE);
            if (chosenLanguageObject){
                console.log(chosenLanguageObject.password);
                const chosenLanguage = chosenLanguageObject.password;
                if (i18n.language !== chosenLanguage){
                    i18n.changeLanguage(chosenLanguage)
                }
            }

            const { data: packsInfo } = await Api.getPacks();
            let packsForUpdate: Pack[] = [];

            const loadedPacksLanguageObject = await Keychain.getInternetCredentials(LOADED_PACKS_LANGUAGE);
            if (loadedPacksLanguageObject && loadedPacksLanguageObject.password !== i18n.language){
                console.log("update packs to " + loadedPacksLanguageObject.password);
                if (packsInfo.length) {
                    packsForUpdate = packsInfo;
                }
            } else {
                const packs = getPacks(true);
                if (packs.length) {
                    for (let i = 0; i < packsInfo.length; i++) {
                        const packInfo = packsInfo[i];
                        const packIndex = packs.findIndex((x) => x.id === packInfo.id);
                        if (packIndex > -1) {
                            if (packs[packIndex].updated_ts < packInfo.updated_ts) {
                                //Newer Pack
                                packsForUpdate.push(packInfo);
                            }
                            packs.splice(packIndex, 1);
                        } else {
                            // New Pack
                            packsForUpdate.push(packInfo);
                        }
                    }

                    if (packs.length) {
                        deletePacks(packs);
                    }
                } else {
                    // Local db is empty
                    if (packsInfo.length) {
                        packsForUpdate = packsInfo;
                    }
                }
            }

            if (packsForUpdate.length) {
                const { data: paths } = await Api.getPacksPaths(packsForUpdate.map(p=> p.id), i18n.language);
                const pathsDict = {}
                paths.forEach((p) => {
                    if (p.packId) {
                        pathsDict[p.packId] = p.path
                    }
                })
                const loadedPacks = await Promise.all<Pack>(
                    packsForUpdate.map(async (packInfoUpdate) => {
                        const resp = await fetch(pathsDict[packInfoUpdate.id]);
                        return resp.json();
                    })
                );
                upsertPacks(loadedPacks);
                await Keychain.setInternetCredentials(LOADED_PACKS_LANGUAGE, LOADED_PACKS_LANGUAGE, i18n.language)
            }
        } catch (exceptionVar) {
            // Log error. Turn on offline mode.
        } finally {
            await RNBootSplash.hide({ fade: true });
            navigation.navigate('Home');
        }
    }, []);

    useEffect(() => {
        const initSplash = async () => {
            await splashScreenHide();
        };
        initSplash().catch(console.error);
    });

    const containerStyle = React.useMemo(() => {
        return {
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center'
        };
    }, []);

    return (
        <View style={containerStyle}>
            <ActivityIndicator size={'large'} color={'#93A3B1'} />
        </View>
    );
}
