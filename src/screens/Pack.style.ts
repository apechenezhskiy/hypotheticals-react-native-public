import { StyleSheet } from 'react-native';
import { AppStyles } from 'hypotheticals-pure/src//AppStyles';

export default StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center'
    },
    main: {
        paddingHorizontal: 10,
        marginVertical: 0,
        width: '100%'
    },
    headerBackgroundScreen: {
        position: 'relative',
        backgroundColor: '#394474',
        borderBottomLeftRadius: 50,
        borderBottomRightRadius: 50,
        width: '100%',
        height: '15%',
        zIndex: 1,
        justifyContent: 'center'
    },
    anotherScreen: {
        backgroundColor: 'transparent',
        width: '98%',
        top: '-17%',
        height: '110%'
    },
    packScreenTitle: {
        color: AppStyles.color.text,
        fontWeight: '400',
        fontSize: 18,
        textAlign: 'center',
        top: '30%'
    },
    scrollviewContent: {
        alignItems: 'center'
    },
    flItem: {
        width: '100%',
        alignSelf: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    verticalCenter: {
        marginTop: 'auto',
        marginBottom: 'auto'
    },
    item: {
        padding: 13,
        marginVertical: 0,
        flexDirection: 'column',
        justifyContent: 'space-between',
        backgroundColor: AppStyles.color.white
    },
    item_title: {
        fontSize: 16,
        color: '#1E3163'
    },
    packInfo: {
        height: 126,
        backgroundColor: AppStyles.color.primary,
        width: '100%',
        paddingBottom: 24,
        paddingTop: 24
    },
    linearGradient: {
        flex: 1,
        justifyContent: 'center',
        width: '100%',
        alignItems: 'center'
    },
    appButtonContainer: {
        elevation: 8,
        backgroundColor: '#009688',
        borderRadius: 10,
        paddingVertical: 10,
        paddingHorizontal: 12,
        width: 100
    },
    answerCardContainer: {
        backgroundColor: 'transparent',
        width: '100%',
        alignItems: 'center'
    },
    answerCardText: {
        fontSize: 24,
        color: '#FFFFFF',
        textAlign: 'center',
        width: '80%'
    },
    answerCardTextOpacity: {
        opacity: 0.3
    },
    answerCardTextTop: {
        paddingTop: '20%'
    },
    answerCardTextBottom: {
        marginBottom: '50%'
    },
    answerCardContainerTop: {
        paddingBottom: '1%'
    },
    answerCardContainerBottom: {
        paddingTop: '1%'
    },
    answerCardTop: {
        borderBottomLeftRadius: 50,
        borderBottomRightRadius: 50,
        height: '100%',
        justifyContent: 'center',
        border: '1px solid #000000',
        boxShadow: '0px 4px 4px rgba(0, 0, 0, 0.25)',
        alignItems: 'center'
    },
    answerCardBottom: {
        borderTopLeftRadius: 50,
        borderTopRightRadius: 50,
        height: '100%',
        justifyContent: 'center',
        border: '1px solid #000000',
        boxShadow: '0px 4px 4px rgba(0, 0, 0, 0.25)',
        alignItems: 'center'
    },
    answersContainer: {
        display: 'flex',
        width: '100%',
        height: '100%',
        backgroundColor: 'transparent'
    },
    buttonsContainer: {
        bottom: 50,
        position: 'absolute',
        backgroundColor: 'transparent',

        flexDirection: 'row',
        justifyContent: 'space-between',
        width: '70%'
    },
    invisible: {
        opacity: 0
    },
    progressContainerTop: {
        bottom: 20,
        position: 'absolute',
        backgroundColor: 'transparent',
        width: '80%',
        alignItems: 'center'
    },
    progressContainerBottom: {
        top: 20,
        position: 'absolute',
        backgroundColor: 'transparent',
        width: '80%',
        alignItems: 'center'
    },
    headerRightIcon: {
        paddingRight: 10,
        paddingTop: 10,
        backgroundColor: 'transparent'
    }
});
