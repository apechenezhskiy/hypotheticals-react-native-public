import { DefaultTheme } from '@react-navigation/native';

type AppStylesType = {
    color: {
        primary: string;
        lightPrimary: string;
        lighterPrimary: string;
        lightAccent: string;
        background: string;
        darkAccent: string;
        text: string;
        secondaryText: string;
        success: string;
        danger: string;
        placeholder: string;
        main: string;
        title: string;
        subtitle: string;
        categoryTitle: string;
        tint: string;
        description: string;
        filterTitle: string;
        starRating: string;
        location: string;
        white: string;
        facebook: string;
        grey: string;
        greenBlue: string;
        blue: string;
    };
    fontSize: {
        title: number;
        content: number;
        normal: number;
    };
    buttonWidth: {
        main: string;
    };
    textInputWidth: {
        main: string;
    };
    borderRadius: {
        main: number;
        small: number;
    };
};

export const AppStyles: AppStylesType = {
    color: {
        primary: '#03a9f4',
        lightPrimary: '#63B9F4',
        lighterPrimary: '#33B9F4',
        lightAccent: '#78B7C9',
        background: '#475490',
        darkAccent: '#91555C',
        text: '#FFFFFF',
        secondaryText: '#757575',
        success: '#3DB07F',
        danger: '#F44336',
        placeholder: '#BDBDBD',
        main: '#5ea23a',
        title: '#464646',
        subtitle: '#545454',
        categoryTitle: '#161616',
        tint: '#ff5a66',
        description: '#bbbbbb',
        filterTitle: '#8a8a8a',
        starRating: '#2bdf85',
        location: '#a9a9a9',
        white: 'white',
        facebook: '#4267b2',
        grey: 'grey',
        greenBlue: '#00aea8',
        blue: '#3293fe'
    },
    fontSize: {
        title: 30,
        content: 20,
        normal: 16
    },
    buttonWidth: {
        main: '70%'
    },
    textInputWidth: {
        main: '80%'
    },
    borderRadius: {
        main: 10,
        small: 5
    }
};

export const CustomTheme = {
    ...DefaultTheme,
    colors: {
        ...DefaultTheme.colors,
        primary: AppStyles.color.primary,
        background: AppStyles.color.background,
        text: AppStyles.color.text,
        card: AppStyles.color.background
    }
};
