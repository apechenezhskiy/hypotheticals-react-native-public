import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import RNLanguageDetector from '@os-team/i18next-react-native-language-detector';
import en from './en.json';
import de from './de.json';
import it from './it.json';
import es from './es.json';
import fr from './fr.json';
import pt from './pt.json';
import ja from './ja.json';
import ru from './ru.json';

const resources = {
    en: {
        translation: en
    },
    de: {
        translation: de
    },
    it: {
        translation: it
    },
    es: {
        translation: es
    },
    fr: {
        translation: fr
    },
    pt: {
        translation: pt
    },
    ja: {
        translation: ja
    },
    ru: {
        translation: ru
    }
};

i18n.use(RNLanguageDetector)
    .use(initReactI18next)
    .init({
        resources,
        fallbackLng: 'en', // fallback language if the value in lng is not available
        supportedLngs: ['en', 'de', 'it', 'es', 'fr', 'pt', 'ja', 'ru'],
        ns: [],
        defaultNS: undefined,
        compatibilityJSON: 'v3',
        interpolation: {
            escapeValue: false
        }
    });
