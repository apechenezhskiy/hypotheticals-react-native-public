import { configureStore, ThunkAction, Action, getDefaultMiddleware } from '@reduxjs/toolkit';
import packsReducer from '../../features/packs/packsSlice';
import questionsReducer from '../../features/question/questionSlice';

import { combineReducers } from 'redux';

//const reducer = combineReducers(counterSlice.reducer);

export const store = configureStore({
    reducer: {
        packs: packsReducer,
        questions: questionsReducer
    },
    middleware: (getDefaultMiddleware) =>
        getDefaultMiddleware({
            serializableCheck: false
        })
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<ReturnType, RootState, unknown, Action<string>>;
