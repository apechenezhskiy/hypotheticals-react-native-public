/**
 * Learn more about Light and Dark modes:
 * https://docs.expo.io/guides/color-schemes/
 */

import React from 'react';
import { Text as DefaultText, View as DefaultView, StyleSheet } from 'react-native';

import Colors from 'hypotheticals-pure/src/constants/Colors';
import useColorScheme from 'hypotheticals-pure/src/hooks/useColorScheme';
import { AppStyles } from 'hypotheticals-pure/src/AppStyles';
import { ThemeProps } from 'hypotheticals-pure/src/types';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        padding: 20
    },
    flashMessage: {
        position: 'absolute',
        backgroundColor: AppStyles.color.danger,
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        height: 40,
        top: 0
    },
    flashMessageText: {
        color: AppStyles.color.background
    },
    defaultTextStyle: {
        margin: 0,
        color: AppStyles.color.danger,
        alignSelf: 'flex-start',
        marginHorizontal: '10%',
        fontSize: 12
    }
});

export function useThemeColor(props: { light?: string; dark?: string }, colorName: keyof typeof Colors.light & keyof typeof Colors.dark) {
    const theme = useColorScheme();
    const colorFromProps = props[theme];

    if (colorFromProps) {
        return colorFromProps;
    } else {
        return Colors[theme][colorName];
    }
}

export type TextProps = ThemeProps & DefaultText['props'];
export type ViewProps = ThemeProps & DefaultView['props'];

export function Text(props: TextProps) {
    const { style, lightColor, darkColor, ...otherProps } = props;
    const color = useThemeColor({ light: lightColor, dark: darkColor }, 'text');

    return <DefaultText style={[{ color }, style]} {...otherProps} />;
}

export function ErrorText({ style, text, ...otherProps }: TextProps) {
    if (text === '') {
        return null;
    }

    return (
        <DefaultText style={[styles.defaultTextStyle, style]} {...otherProps}>
            {text}
        </DefaultText>
    );
}

export function View(props: ViewProps) {
    const { style, lightColor, darkColor, ...otherProps } = props;
    const backgroundColor = useThemeColor({ light: AppStyles.color.background, dark: darkColor }, 'background');

    return <DefaultView style={[{ backgroundColor }, style]} {...otherProps} />;
}

export function BaseView(props: ViewProps) {
    return <View style={styles.container} {...props} />;
}
