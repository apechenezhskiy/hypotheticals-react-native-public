import * as React from 'react';
import { View, Text } from 'hypotheticals-pure/src//components/Themed';

import styles from './ProgressBar.style';

const ProgressBar = ({ completed, isLeft }: { completed: number; isLeft: boolean }) => {
    const fillerStyle = React.useCallback(() => {
        return [
            styles.fillerStyles,
            {
                width: `${completed}%`,
                backgroundColor: isLeft ? '#EA3966' : '#565ca3',
                alignSelf: isLeft ? 'flex-start' : 'flex-end'
            }
        ];
    }, [completed, isLeft]);

    const labelStyles = React.useCallback(() => {
        return [
            styles.labelStyles,
            {
                alignSelf: isLeft ? 'flex-end' : 'flex-start'
            }
        ];
    }, [isLeft]);

    return (
        <View style={styles.containerStyles}>
            <View style={fillerStyle()}>
                <Text style={labelStyles()}>{`${completed}%`}</Text>
            </View>
        </View>
    );
};

export default ProgressBar;
