import * as React from 'react';
import Svg, { Path } from 'react-native-svg';

const LockedSVG = (props) => (
    <Svg viewBox={'0 0 48 48'} xmlns={'http://www.w3.org/2000/svg'} height={40} width={40} {...props}>
        <Path d={'M3.980 16.360 L31.480 16.360 L31.480 38.360 L3.980 38.360 Z'} fill={'#ffe500'} />
        <Path
            d={'M3.980 16.360 L31.480 16.360 L31.480 38.360 L3.980 38.360 Z'}
            fill={'none'}
            stroke={'#45413c'}
            strokeLinecap={'round'}
            strokeLinejoin={'round'}
        />
        <Path
            d={'M17.73,1.36A10.76,10.76,0,0,0,7,12.11v4.25h4.5V12.11a6.25,6.25,0,0,1,12.5,0v4.25h4.5V12.11A10.75,10.75,0,0,0,17.73,1.36Z'}
            fill={'#daedf7'}
        />
        <Path d={'M17.73,1.36A10.76,10.76,0,0,0,7,12.11v2.5a10.75,10.75,0,0,1,21.5,0v-2.5A10.75,10.75,0,0,0,17.73,1.36Z'} fill={'#ffffff'} />
        <Path
            d={'M17.73,1.36A10.76,10.76,0,0,0,7,12.11v4.25h4.5V12.11a6.25,6.25,0,0,1,12.5,0v4.25h4.5V12.11A10.75,10.75,0,0,0,17.73,1.36Z'}
            fill={'none'}
            stroke={'#45413c'}
            strokeLinecap={'round'}
            strokeLinejoin={'round'}
        />
    </Svg>
);

export default LockedSVG;
