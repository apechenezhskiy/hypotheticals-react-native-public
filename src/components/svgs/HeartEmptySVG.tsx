import * as React from 'react';
import Svg, { Path } from 'react-native-svg';

const HeartEmptySVG = (props) => (
    <Svg viewBox={'0 0 48 48'} height={60} width={60} xmlns={'http://www.w3.org/2000/svg'} {...props}>
        <Path d={'M12.500 45.500 A11.5 1.5 0 1 0 35.500 45.500 A11.5 1.5 0 1 0 12.500 45.500 Z'} fill={'#45413c'} opacity={0.15} />
        <Path
            d={
                'M24,11.93a10,10,0,0,1,10-10c7,0,10,7,10,12.5C44,29,30.17,38.35,25.51,41.09a3,3,0,0,1-3,0C17.83,38.35,4,29,4,14.43,4,8.9,7,1.93,14,1.93A10,10,0,0,1,24,11.93Z'
            }
            fill={'none'}
            stroke={'#45413c'}
            strokeLinecap={'round'}
            strokeLinejoin={'round'}
        />
    </Svg>
);

export default HeartEmptySVG;
