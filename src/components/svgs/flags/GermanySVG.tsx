import * as React from 'react';
import Svg, { Path } from 'react-native-svg';

const GermanySVG = (props) => (
    <Svg
    viewBox="-0.5 -0.5 48 48"
    xmlns="http://www.w3.org/2000/svg"
    height={64}
    width={64}
    {...props}
  >
    <Path
      d="M44.063 11.995h-6.188A44.934 44.934 0 0 1 23.5 9.674 44.777 44.777 0 0 0 9.175 7.344H2.938c-0.568 0 -0.979 0.343 -0.979 0.774v25.458a0.92 0.92 0 0 0 0.979 0.774h6.237A45.042 45.042 0 0 1 23.5 36.68a44.67 44.67 0 0 0 14.345 2.33H44.063a0.911 0.911 0 0 0 0.979 -0.774V12.729a0.92 0.92 0 0 0 -0.979 -0.734Z"
      fill="#ffe500"
      stroke="#45413c"
      strokeLinecap="round"
      strokeLinejoin="round"
      strokeWidth={1}
    />
    <Path
      d="M1.958 25.615h7.216A45.042 45.042 0 0 1 23.5 27.936h0a44.67 44.67 0 0 0 14.345 2.33h7.216v-9.527h-7.187A44.934 44.934 0 0 1 23.5 18.418h0a44.777 44.777 0 0 0 -14.325 -2.33H1.958Z"
      fill="#ff6242"
      stroke="#45413c"
      strokeLinecap="round"
      strokeLinejoin="round"
      strokeWidth={1}
    />
    <Path
      d="M44.063 11.995h-6.188A44.934 44.934 0 0 1 23.5 9.674 44.777 44.777 0 0 0 9.175 7.344H2.938c-0.568 0 -0.979 0.343 -0.979 0.774v7.97h7.216A44.777 44.777 0 0 1 23.5 18.418a44.934 44.934 0 0 0 14.345 2.321h7.216V12.729a0.92 0.92 0 0 0 -0.999 -0.734Z"
      fill="#656769"
      stroke="#45413c"
      strokeLinecap="round"
      strokeLinejoin="round"
      strokeWidth={1}
    />
    <Path
      d="M12.24 44.552a11.26 1.469 0 1 0 22.521 0 11.26 1.469 0 1 0 -22.521 0Z"
      fill="#45413c"
      opacity={0.15}
      strokeWidth={1}
    />
  </Svg>
);

export default GermanySVG;
