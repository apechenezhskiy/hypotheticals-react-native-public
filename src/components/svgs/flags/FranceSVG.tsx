import * as React from 'react';
import Svg, { Path } from 'react-native-svg';

const FranceSVG = (props) => (
    <Svg
    viewBox="-0.5 -0.5 48 48"
    xmlns="http://www.w3.org/2000/svg"
    height={64}
    width={64}
    {...props}
  >
    <Path
      d="M44.063 11.995h-6.188A44.934 44.934 0 0 1 23.5 9.674 44.777 44.777 0 0 0 9.175 7.344H2.938c-0.568 0 -0.979 0.343 -0.979 0.774v25.458a0.92 0.92 0 0 0 0.979 0.774h6.237A45.042 45.042 0 0 1 23.5 36.68a44.67 44.67 0 0 0 14.345 2.33H44.063a0.911 0.911 0 0 0 0.979 -0.774V12.729a0.92 0.92 0 0 0 -0.979 -0.734Z"
      fill="#ff6242"
      stroke="#45413c"
      strokeLinecap="round"
      strokeLinejoin="round"
      strokeWidth={1}
    />
    <Path
      d="M23.5 9.674A42.662 42.662 0 0 0 15.941 7.833v27.035A43.514 43.514 0 0 1 23.5 36.68a42.359 42.359 0 0 0 7.579 1.821V11.486A43.191 43.191 0 0 1 23.5 9.674Z"
      fill="#ffffff"
      stroke="#45413c"
      strokeWidth={1}
    />
    <Path
      d="M9.175 7.344H2.938c-0.568 0 -0.979 0.343 -0.979 0.774v25.458a0.92 0.92 0 0 0 0.979 0.774h6.237a45.688 45.688 0 0 1 6.766 0.509V7.833a45.688 45.688 0 0 0 -6.766 -0.49Z"
      fill="#009fd9"
      stroke="#45413c"
      strokeWidth={1}
    />
    <Path
      d="M12.24 44.552a11.26 1.469 0 1 0 22.521 0 11.26 1.469 0 1 0 -22.521 0Z"
      fill="#45413c"
      opacity={0.15}
      strokeWidth={1}
    />
  </Svg>
);

export default FranceSVG;
