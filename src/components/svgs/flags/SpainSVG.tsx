import * as React from 'react';
import Svg, { Path } from 'react-native-svg';

const SpainSVG = (props) => (
    <Svg
    viewBox="-0.5 -0.5 48 48"
    xmlns="http://www.w3.org/2000/svg"
    height={64}
    width={64}
    {...props}
  >
    <Path
      d="M44.063 11.995h-6.188A44.934 44.934 0 0 1 23.5 9.674 44.777 44.777 0 0 0 9.175 7.344H2.938c-0.568 0 -0.979 0.343 -0.979 0.774v25.458a0.92 0.92 0 0 0 0.979 0.774h6.237A45.042 45.042 0 0 1 23.5 36.68a44.67 44.67 0 0 0 14.345 2.33H44.063a0.911 0.911 0 0 0 0.979 -0.774V12.729a0.92 0.92 0 0 0 -0.979 -0.734Z"
      fill="#ffe500"
      stroke="#45413c"
      strokeLinecap="round"
      strokeLinejoin="round"
      strokeWidth={1}
    />
    <Path
      d="M12.729 25.801c0.871 0.069 1.733 0.157 2.595 0.274a1.762 1.762 0 0 0 1.958 -1.753v-4.299a46.677 46.677 0 0 0 -6.208 -0.695v4.71a1.762 1.762 0 0 0 1.655 1.762Z"
      fill="#ffffff"
      stroke="#45413c"
      strokeLinecap="round"
      strokeLinejoin="round"
      strokeWidth={1}
    />
    <Path
      d="M45.091 32.636h-7.216A44.66 44.66 0 0 1 23.5 30.354a44.777 44.777 0 0 0 -14.325 -2.37H1.958v5.601a0.92 0.92 0 0 0 0.979 0.774h6.237A45.042 45.042 0 0 1 23.5 36.68a44.67 44.67 0 0 0 14.345 2.33H44.063a0.911 0.911 0 0 0 0.979 -0.774Z"
      fill="#e04122"
      stroke="#45413c"
      strokeLinecap="round"
      strokeLinejoin="round"
      strokeWidth={1}
    />
    <Path
      d="M45.091 12.729a0.92 0.92 0 0 0 -0.979 -0.774h-6.237A44.934 44.934 0 0 1 23.5 9.674 44.777 44.777 0 0 0 9.175 7.344H2.938c-0.568 0 -0.979 0.343 -0.979 0.774V13.708h7.216A45.042 45.042 0 0 1 23.5 16.039a44.67 44.67 0 0 0 14.345 2.33h7.216Z"
      fill="#e04122"
      stroke="#45413c"
      strokeLinecap="round"
      strokeLinejoin="round"
      strokeWidth={1}
    />
    <Path
      d="M8.078 19.27h1.479v5.836H8.078z"
      fill="#ffffff"
      stroke="#45413c"
      strokeLinecap="round"
      strokeLinejoin="round"
      strokeWidth={1}
    />
    <Path
      d="m20.337 26.477 -1.469 -0.382 0 -5.836 1.469 0.382 0 5.836z"
      fill="#ffffff"
      stroke="#45413c"
      strokeLinecap="round"
      strokeLinejoin="round"
      strokeWidth={1}
    />
    <Path
      d="M15.304 26.075a1.762 1.762 0 0 0 1.958 -1.753v-1.302c-0.979 -0.176 -2.027 -0.323 -3.045 -0.441v3.368Z"
      fill="#e04122"
      stroke="#45413c"
      strokeLinecap="round"
      strokeLinejoin="round"
      strokeWidth={1}
    />
    <Path
      d="M14.266 19.583c-1.048 -0.117 -2.105 -0.196 -3.163 -0.245v2.986c1.058 0 2.115 0.127 3.163 0.245Z"
      fill="#e04122"
      stroke="#45413c"
      strokeLinecap="round"
      strokeLinejoin="round"
      strokeWidth={1}
    />
    <Path
      d="M12.778 17.919a23.363 23.363 0 0 1 3.114 0.382s1.283 -0.421 0.832 -1.332c-0.656 -1.302 -2.438 -0.539 -2.438 -0.539s-1.381 -1.067 -2.232 -0.255 0.725 1.743 0.725 1.743Z"
      fill="#e04122"
      stroke="#45413c"
      strokeLinecap="round"
      strokeLinejoin="round"
      strokeWidth={1}
    />
    <Path
      d="M17.312 22.305a3.368 3.368 0 0 1 3.026 0.392v1.958a3.368 3.368 0 0 0 -3.026 -0.392Z"
      fill="#e04122"
      stroke="#45413c"
      strokeLinecap="round"
      strokeLinejoin="round"
      strokeWidth={1}
    />
    <Path
      d="M8.078 21.287a3.368 3.368 0 0 1 3.026 0.392v1.958a3.368 3.368 0 0 0 -3.026 -0.392Z"
      fill="#e04122"
      stroke="#45413c"
      strokeLinecap="round"
      strokeLinejoin="round"
      strokeWidth={1}
    />
    <Path
      d="M12.24 44.552a11.26 1.469 0 1 0 22.521 0 11.26 1.469 0 1 0 -22.521 0Z"
      fill="#45413c"
      opacity={0.15}
      strokeWidth={1}
    />
  </Svg>
);

export default SpainSVG;
