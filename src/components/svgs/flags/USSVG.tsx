import * as React from 'react';
import Svg, { Path } from 'react-native-svg';

const USSVG = (props) => (
    <Svg
    viewBox="0 0 48 48"
    xmlns="http://www.w3.org/2000/svg"
    height={64}
    width={64}
    {...props}
  >
    <Path
      d="M24 9.53h0A48.85 48.85 0 0 0 8.37 7H1.61a1 1 0 0 0-1.11.84v27.75a1 1 0 0 0 1.11.85h6.76A48.85 48.85 0 0 1 24 39h0a48.85 48.85 0 0 0 15.63 2.5h6.76c.61 0 1.11-.37 1.11-.84V12.91a1 1 0 0 0-1.11-.84h-6.76A48.58 48.58 0 0 1 24 9.53Z"
      fill="#ffffff"
      stroke="#45413c"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <Path
      d="M47.5 16.29h-7.87A48.85 48.85 0 0 1 24 13.76h0a48.85 48.85 0 0 0-15.63-2.54H.5V7.84A1 1 0 0 1 1.61 7h6.76A48.85 48.85 0 0 1 24 9.53h0a48.58 48.58 0 0 0 15.63 2.54h6.76a1 1 0 0 1 1.11.84Z"
      fill="#ff6242"
      stroke="#45413c"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <Path
      d="M47.5 20.52h-7.87A48.85 48.85 0 0 1 24 18h0a48.85 48.85 0 0 0-15.63-2.55H.5v4.22h7.87A48.85 48.85 0 0 1 24 22.21h0a48.85 48.85 0 0 0 15.63 2.53h7.87Z"
      fill="#ff6242"
      stroke="#45413c"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <Path
      d="M47.5 28.83h-7.87A48.85 48.85 0 0 1 24 26.3h0a48.85 48.85 0 0 0-15.63-2.54H.5V28h7.87A48.85 48.85 0 0 1 24 30.52h0a48.85 48.85 0 0 0 15.63 2.54h7.87Z"
      fill="#ff6242"
      stroke="#45413c"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <Path
      d="M46.39 41.5c.61 0 1.11-.37 1.11-.84v-3.38h-7.87A48.85 48.85 0 0 1 24 34.75h0a48.58 48.58 0 0 0-15.63-2.54H.5v3.38a1 1 0 0 0 1.11.85h6.76A48.85 48.85 0 0 1 24 39h0a48.85 48.85 0 0 0 15.63 2.5Z"
      fill="#ff6242"
      stroke="#45413c"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <Path
      d="M19.3 8.22A49.69 49.69 0 0 0 8.37 7H1.61a1 1 0 0 0-1.11.84v11.83h7.87a49.69 49.69 0 0 1 10.93 1.22Z"
      fill="#00b8f0"
      stroke="#45413c"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <Path
      d="m4.72 9.46.29.59a.16.16 0 0 0 .12.09l.66.1a.16.16 0 0 1 .09.27L5.4 11a.2.2 0 0 0 0 .15l.11.65a.17.17 0 0 1-.24.17l-.58-.31a.19.19 0 0 0-.15 0l-.59.31a.16.16 0 0 1-.23-.17l.11-.65a.17.17 0 0 0 0-.15l-.47-.46a.16.16 0 0 1 .09-.27l.65-.1a.18.18 0 0 0 .13-.09l.29-.59a.16.16 0 0 1 .2-.03Z"
      fill="#ffffff"
    />
    <Path
      d="m9.52 9.46.29.59a.18.18 0 0 0 .12.09l.66.1a.16.16 0 0 1 .09.27l-.48.49a.2.2 0 0 0 0 .15l.11.65a.16.16 0 0 1-.23.17l-.59-.31a.19.19 0 0 0-.15 0l-.59.31a.16.16 0 0 1-.23-.17l.11-.65a.2.2 0 0 0 0-.15l-.48-.46a.16.16 0 0 1 .09-.27l.66-.1a.18.18 0 0 0 .12-.09l.29-.59a.16.16 0 0 1 .21-.03Z"
      fill="#ffffff"
    />
    <Path
      d="m14.29 10.45.29.59a.16.16 0 0 0 .12.09l.65.09a.16.16 0 0 1 .09.28L15 12a.16.16 0 0 0-.05.14l.12.66a.17.17 0 0 1-.24.17l-.58-.31a.14.14 0 0 0-.15 0l-.59.31a.16.16 0 0 1-.23-.17l.11-.66a.16.16 0 0 0 0-.14l-.47-.46a.16.16 0 0 1 .09-.28l.65-.09a.16.16 0 0 0 .04-.17l.3-.59a.16.16 0 0 1 .29.04Z"
      fill="#ffffff"
    />
    <Path
      d="m4.72 14.19.29.59a.16.16 0 0 0 .12.09l.66.09a.17.17 0 0 1 .09.28l-.48.46a.18.18 0 0 0 0 .14l.11.65a.16.16 0 0 1-.24.17l-.58-.3a.14.14 0 0 0-.15 0l-.59.3a.16.16 0 0 1-.23-.17l.11-.65a.16.16 0 0 0 0-.14l-.47-.46a.17.17 0 0 1 0-.24l.64-.13a.18.18 0 0 0 .13-.09l.29-.59a.16.16 0 0 1 .3 0Z"
      fill="#ffffff"
    />
    <Path
      d="m9.52 14.19.29.59a.18.18 0 0 0 .12.09l.66.09a.17.17 0 0 1 .09.28l-.48.46a.18.18 0 0 0 0 .14l.11.65a.16.16 0 0 1-.23.17l-.59-.3a.14.14 0 0 0-.15 0l-.59.3a.16.16 0 0 1-.23-.17l.11-.65a.18.18 0 0 0 0-.14l-.48-.46a.17.17 0 0 1 .01-.24l.66-.09a.18.18 0 0 0 .12-.09l.29-.59a.16.16 0 0 1 .29-.04Z"
      fill="#ffffff"
    />
    <Path
      d="m14.29 15.17.29.6a.15.15 0 0 0 .12.08l.65.1a.15.15 0 0 1 .09.27l-.47.47a.15.15 0 0 0-.05.14l.12.65a.17.17 0 0 1-.24.17l-.58-.31a.19.19 0 0 0-.15 0l-.59.31a.16.16 0 0 1-.23-.17l.11-.65a.15.15 0 0 0 0-.14l-.47-.47a.15.15 0 0 1 .09-.27l.65-.1a.15.15 0 0 0 .12-.08l.3-.6a.16.16 0 0 1 .24 0Z"
      fill="#ffffff"
    />
    <Path
      d="M11 45.5a11.5 1.5 0 1 0 23 0 11.5 1.5 0 1 0-23 0Z"
      fill="#45413c"
      opacity={0.15}
    />
  </Svg>
);

export default USSVG;
