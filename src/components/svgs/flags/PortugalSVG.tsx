import * as React from 'react';
import Svg, { Path } from 'react-native-svg';

const PortugalSVG = (props) => (
    <Svg
    viewBox="-0.5 -0.5 48 48"
    xmlns="http://www.w3.org/2000/svg"
    height={64}
    width={64}
    {...props}
  >
    <Path
      d="M44.063 11.505h-6.188A44.934 44.934 0 0 1 23.5 9.185 44.777 44.777 0 0 0 9.175 6.854H2.938c-0.568 0 -0.979 0.343 -0.979 0.774v25.458a0.92 0.92 0 0 0 0.979 0.774h6.237A45.042 45.042 0 0 1 23.5 36.229a44.67 44.67 0 0 0 14.345 2.33H44.063a0.911 0.911 0 0 0 0.979 -0.774v-25.458a0.92 0.92 0 0 0 -0.979 -0.822Z"
      fill="#e04122"
      stroke="#45413c"
      strokeLinecap="round"
      strokeLinejoin="round"
      strokeWidth={1}
    />
    <Path
      d="M15.853 7.354A45.825 45.825 0 0 0 9.175 6.854H2.938c-0.568 0 -0.979 0.343 -0.979 0.774v25.458a0.92 0.92 0 0 0 0.979 0.774h6.237a45.825 45.825 0 0 1 6.678 0.49Z"
      fill="#46b000"
      stroke="#45413c"
      strokeLinecap="round"
      strokeLinejoin="round"
      strokeWidth={1}
    />
    <Path
      d="M9.41 24.644a8.156 7.393 60.71 1 0 12.895 -7.233 8.156 7.393 60.71 1 0 -12.896 7.233Z"
      fill="#ffe500"
      stroke="#45413c"
      strokeLinecap="round"
      strokeLinejoin="round"
      strokeWidth={1}
    />
    <Path
      d="M23.128 19.407a48.214 48.214 0 0 0 -14.198 -2.223"
      fill="#e04122"
      stroke="#45413c"
      strokeLinecap="round"
      strokeLinejoin="round"
      strokeWidth={1}
    />
    <Path
      d="M23.343 22.873a69.629 69.629 0 0 0 -15.04 -1.841"
      fill="#e04122"
      stroke="#45413c"
      strokeLinecap="round"
      strokeLinejoin="round"
      strokeWidth={1}
    />
    <Path
      d="M21.835 26.379a44.973 44.973 0 0 0 -12.494 -1.811"
      fill="#e04122"
      stroke="#45413c"
      strokeLinecap="round"
      strokeLinejoin="round"
      strokeWidth={1}
    />
    <Path
      d="m19.074 17.106 -6.443 -0.558a0.881 0.881 0 0 0 -0.979 0.891v3.554A6.032 6.032 0 0 0 15.314 26.438a1.674 1.674 0 0 0 1.087 0.088 4.896 4.896 0 0 0 3.652 -4.798v-3.554a1.087 1.087 0 0 0 -0.979 -1.067Z"
      fill="#e04122"
      stroke="#45413c"
      strokeWidth={1}
    />
    <Path
      d="M17.997 18.526 13.708 18.144a0.45 0.45 0 0 0 -0.49 0.45v2.663a3.78 3.78 0 0 0 2.291 3.407 1.048 1.048 0 0 0 0.685 0.059 3.045 3.045 0 0 0 2.291 -3.006v-2.663a0.529 0.529 0 0 0 -0.49 -0.529Z"
      fill="#ffffff"
      strokeWidth={1}
    />
    <Path
      d="M15.765 22.276a0.274 0.274 0 0 0 0.176 0 0.783 0.783 0 0 0 0.587 -0.774v-0.725l-1.351 -0.117v0.725a0.979 0.979 0 0 0 0.587 0.891Z"
      fill="#009fd9"
      strokeWidth={1}
    />
    <Path
      d="M14.129 22.129a0.245 0.245 0 0 0 0.166 0 0.783 0.783 0 0 0 0.587 -0.774v-0.725L13.542 20.563v0.725a0.979 0.979 0 0 0 0.587 0.842Z"
      fill="#009fd9"
      strokeWidth={1}
    />
    <Path
      d="M17.41 22.423a0.274 0.274 0 0 0 0.176 0 0.774 0.774 0 0 0 0.587 -0.764v-0.725l-1.351 -0.117V21.542a0.979 0.979 0 0 0 0.587 0.881Z"
      fill="#009fd9"
      strokeWidth={1}
    />
    <Path
      d="M15.765 20.445a0.274 0.274 0 0 0 0.176 0 0.774 0.774 0 0 0 0.587 -0.764v-0.734l-1.351 -0.117V19.583a0.979 0.979 0 0 0 0.587 0.862Z"
      fill="#009fd9"
      strokeWidth={1}
    />
    <Path
      d="M15.765 24.225a0.274 0.274 0 0 0 0.176 0 0.783 0.783 0 0 0 0.587 -0.774v-0.725l-1.351 -0.117v0.725a0.979 0.979 0 0 0 0.587 0.891Z"
      fill="#009fd9"
      strokeWidth={1}
    />
    <Path
      d="M12.24 44.552a11.26 1.469 0 1 0 22.521 0 11.26 1.469 0 1 0 -22.521 0Z"
      fill="#45413c"
      opacity={0.15}
      strokeWidth={1}
    />
  </Svg>
);

export default PortugalSVG;
