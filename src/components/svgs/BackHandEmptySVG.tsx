import * as React from 'react';
import Svg, { Path } from 'react-native-svg';

const BackHandEmptySVG = (props) => (
    <Svg viewBox={'0 0 48 48'} height={60} width={60} xmlns={'http://www.w3.org/2000/svg'} {...props}>
        <Path
            d={
                'M26.73,36.22a2.6,2.6,0,0,0-2.6-2.61h2.6a2.78,2.78,0,1,0,0-5.56,2.78,2.78,0,0,0,0-5.56H39.14a2.42,2.42,0,1,0,0-4.84H20.68a8.15,8.15,0,0,0,4.76-1.54,3.93,3.93,0,0,0,1.32-5,1.56,1.56,0,0,0-2.16-.66l-4,2.2A26.64,26.64,0,0,1,14.73,15a12.28,12.28,0,0,0-7.12,5.31,12.54,12.54,0,0,0-2,6.75v4.13a7.6,7.6,0,0,0,7.6,7.6H24.13A2.59,2.59,0,0,0,26.73,36.22Z'
            }
            fill={'none'}
            stroke={'#45413c'}
            strokeLinecap={'round'}
            strokeLinejoin={'round'}
        />
        <Path d={'M26.73 28.05L24.64 28.05'} fill={'none'} stroke={'#45413c'} strokeLinecap={'round'} strokeLinejoin={'round'} />
        <Path d={'M26.73 22.49L24.64 22.49'} fill={'none'} stroke={'#45413c'} strokeLinecap={'round'} strokeLinejoin={'round'} />
        <Path d={'M20.68 17.65L18.82 17.65'} fill={'none'} stroke={'#45413c'} strokeLinecap={'round'} strokeLinejoin={'round'} />
        <Path d={'M9.000 45.500 A15 1.5 0 1 0 39.000 45.500 A15 1.5 0 1 0 9.000 45.500 Z'} fill={'#45413c'} opacity={0.15} />
    </Svg>
);

export default BackHandEmptySVG;
