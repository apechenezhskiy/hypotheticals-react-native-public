import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    containerStyles: {
        height: 20,
        width: '100%',
        backgroundColor: '#e0e0de',
        borderRadius: 50,
        margin: 50,
        marginBottom: 0,
        marginTop: 0
    },
    fillerStyles: {
        height: 20,
        borderRadius: 50
    },
    labelStyles: {
        color: 'white',
        fontWeight: 'bold',
        paddingRight: 8,
        paddingLeft: 8
    }
});
