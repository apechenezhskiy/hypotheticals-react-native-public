import Realm from 'realm';
import { Pack } from './Pack';

export class Question extends Realm.Object {
    constructor(
        id: string,
        option_1: string,
        option_2: string,
        option_1_stats: number,
        option_2_stats: number,
        created_date: Date,
        pack: Pack,
        answer: boolean,
        isFavourite: boolean,
        order: number,
        isReported: boolean
    ) {
        super();
        this.id = id;
        this.option_1 = option_1;
        this.option_2 = option_2;
        this.option_1_stats = option_1_stats;
        this.option_2_stats = option_2_stats;
        this.created_date = created_date;
        this.pack = pack;
        this.answer = answer;
        this.isFavourite = isFavourite;
        this.order = order;
        this.isReported = isReported;
    }

    id: string;

    option_1: string;

    option_2: string;

    option_1_stats: number;

    option_2_stats: number;

    created_date!: Date;

    pack: Pack;

    answer: boolean;

    isFavourite!: boolean;

    order: number;

    isReported!: boolean;

    static schema = {
        name: 'Question',
        properties: {
            id: 'string',
            option_1: 'string',
            option_2: 'string',
            option_1_stats: 'int',
            option_2_stats: 'int',
            created_date: { type: 'date', default: new Date() },
            pack: {
                type: 'linkingObjects',
                objectType: 'Pack',
                property: 'questions'
            },
            answer: 'bool?',
            isFavourite: 'bool?',
            order: 'int',
            isReported: 'bool?'
        },
        primaryKey: 'id'
    };
}
