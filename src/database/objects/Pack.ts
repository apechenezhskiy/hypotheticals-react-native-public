import Realm from 'realm';
import { Question } from './Question';

export class Pack extends Realm.Object {
    constructor(
        id: string,
        name: string,
        description: string,
        questions: Realm.List<Question>,
        color: { colorFrom: string; colorTo: string },
        updated_ts: number,
        order: number,
        svg_image: string,
        appId: string,
        textsJson: {
            "en": string,
            "ru": string,
            "de": string,
            "it": string,
            "es": string,
            "fr": string,
            "pt": string,
            "ja": string
        }
    ) {
        super();
        this.id = id;
        this.name = name;
        this.description = description;
        this.questions = questions;
        this.color = color;
        this.updated_ts = updated_ts;
        this.order = order;
        this.svg_image = svg_image;
        this.appId = appId;
        this.textsJson = textsJson;
    }

    id: string;

    name: string;

    description: string;

    questions: Realm.List<Question>;

    color: { colorFrom: string; colorTo: string };

    updated_ts: number;

    order: number;

    svg_image: string;

    appId?: string;

    textsJson?: {
        "en": string,
        "ru": string,
        "de": string,
        "it": string,
        "es": string,
        "fr": string,
        "pt": string,
        "ja": string
    }


    static schema = {
        name: 'Pack',
        properties: {
            id: 'string',
            name: 'string',
            description: 'string',
            questions: 'Question[]',
            color: '{}',
            updated_ts: 'int',
            order: 'int',
            svg_image: 'string',
            appId: 'string',
            textsJson: '{}'
        },
        primaryKey: 'id'
    };
}
