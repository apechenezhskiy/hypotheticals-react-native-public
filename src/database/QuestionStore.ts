import realm from 'hypotheticals-pure/src/database/realm';
import { Question } from './objects';

const getQuestions = () => {
    return realm.objects('Question').sorted('order');
};

const getQuestionById = (questionId: string) => {
    return realm.objectForPrimaryKey('Question', questionId);
};

const getQuestionIndexInPack = (packId: string, questionId: string) => {
    let questions: Question[];
    if (packId === '-1') {
        questions = realm.objects('Question').filtered('isFavourite == true');
    } else {
        questions = realm.objects('Question').filtered('pack.id == $0', packId).sorted('order');
    }
    return questionId ? questions.findIndex((q) => q.id === questionId) : 0;
};

const getNextQuestionByIndex = (questions: Question[], prevQuestionId) => {
    const prevQuestionIndex = prevQuestionId ? questions.findIndex((q) => q.id === prevQuestionId) : -1;
    if (questions.length === prevQuestionIndex + 1) {
        return questions[0];
    } else {
        return questions[prevQuestionIndex + 1];
    }
};

const getQuestionByPackId = (packId, prevQuestionId): Question => {
    let questions: Question[];
    if (packId === '-1') {
        questions = realm.objects('Question').filtered('isFavourite == true && answer == null && isReported != true');
        if (questions.length === 0) {
            questions = realm.objects('Question').filtered('isFavourite == true && isReported != true');
            return getNextQuestionByIndex(questions, prevQuestionId);
        }
    } else {
        questions = realm.objects('Question').filtered('pack.id == $0 && answer == null && isReported != true', packId).sorted('order');
        if (questions.length === 0) {
            questions = realm.objects('Question').filtered('pack.id == $0 && isReported != true', packId).sorted('order');
            return getNextQuestionByIndex(questions, prevQuestionId);
        }
    }
    return questions.length ? questions[0] : undefined;
};

const getFavQuestions = () => {
    return realm.objects('Question').filtered('isFavourite == true && isReported != true');
};

const answerQuestions = (id: string, answer: boolean) => {
    realm.write(() => {
        const question: Question = realm.objectForPrimaryKey('Question', id);
        question.answer = answer;
    });
};

const changeFavStatus = (id: string) => {
    realm.write(() => {
        const question = realm.objectForPrimaryKey('Question', id);
        question.isFavourite = !question.isFavourite;
    });
};

const contentReport = (id: string) => {
    realm.write(() => {
        const question = realm.objectForPrimaryKey('Question', id);
        question.isReported = true;
    });
};

export {
    getQuestions,
    getQuestionByPackId,
    answerQuestions,
    changeFavStatus,
    getFavQuestions,
    getQuestionIndexInPack,
    getQuestionById,
    contentReport
};
