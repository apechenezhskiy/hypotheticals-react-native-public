import Realm from 'realm';
import { Pack, Question } from './objects';

//console.log('REALM PATH', Realm.defaultPath);

export default new Realm({ schema: [Pack, Question], schemaVersion: 29 });
