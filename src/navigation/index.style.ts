import { StyleSheet } from 'react-native';
import { AppStyles } from 'hypotheticals-pure/src//AppStyles';

export default StyleSheet.create({
    headerTitleText: {
        color: AppStyles.color.text,
        fontWeight: '400',
        fontSize: 23
    },
    backButtonImageContainer: {
        backgroundColor: 'transparent',
        zIndex: 333,
        width: 50,
        height: 50
    },
    backButtonImage: {
        height: 28,
        width: 28,
        left: 20,
        top: 15
    }
});
