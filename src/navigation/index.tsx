/**
 * If you are not familiar with React Navigation, check out the "Fundamentals" guide:
 * https://reactnavigation.org/docs/getting-started
 *
 */
import 'react-native-gesture-handler';
import { NavigationContainer, useNavigationContainerRef } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { View } from 'hypotheticals-pure/src/components/Themed';
import React from 'react';
import { ColorSchemeName, Image } from 'react-native';
import HomeScreen from 'hypotheticals-pure/src/screens/Home';
import PackScreen from 'hypotheticals-pure/src/screens/Pack';
import SplashScreen from 'hypotheticals-pure/src/screens/Splash';
import OnboardingScreen from 'hypotheticals-pure/src/screens/Onboarding';
import { RootStackParamList } from 'hypotheticals-pure/src/types';
import { CustomTheme, AppStyles } from 'hypotheticals-pure/src/AppStyles';
import styles from './index.style';

import 'hypotheticals-pure//src/locales/index';
import {useTranslation} from 'react-i18next';

function RootNavigator() {
    // A root stack navigator is often used for displaying modals on top of all other content
    // Read more here: https://reactnavigation.org/docs/modal
    const Stack = createStackNavigator<RootStackParamList>();
    const {t} = useTranslation();

    const imageSource = `data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAAAXNSR0IArs4c6QAAA/xJREFUaEPVmVuoVVUUhr+/oqLEKDQp7GJJmGiQkRJCN8iHHoIwk25YQQXRiykagdSDEZmB0IOU3Sjq4VRUoFAESUVERaBlVzIjiO5BZZbd/hgy12a63fusdfattcbLOZs151j/v8ecY/xjbNFwU8PxU0rA9lLgHuA5ScvrRnhcArZnA+8BByfgJ0n6sk4kuhKwfRjwGTA9ATYwVdKPTSHwGLAsA/uUpKvqBD6wdIyA7YuBLRnYb4AZkv6oPQHbU4AvgCMT2H+BeZK21w18xwjYfhU4NwO7RtLaOoI/gIDtlcC9Gdht6duPC1xLa90B2zOBj4BDEtI47ydL+raWyBOofQRsH5pS5gkZ2NeAj4cIfg+wC9gBvCPp117eVRBYD6zoxcGA9kSieAPYCIxJ+qeq34LAW8D8qpuGvO4r4A7gcUl/lb2rILAIeKls8Yifx9G6RFIcs66WX+JVSbQVi+NCz51IOKsStD0ZOBY4DjgPuBQ4s0NhjURyjaRnuvnOCcT/rwMLs8XrJK2uCqyfdbZPAe4EQq4clPmKFL5M0hOd/O8nJWxPBT4HJqXFsXmhpDf7ATeRvbZPA14AZmX74pIvlvR8u68DtFAHHfQdcKqk3RMB0s/apIQfaBOTfwOnSwqF3LJuYu5R4Nps3bOSLusHVC97bT8I3JDtjbo0W1JLGXQjEL3Ap8CJ2ebLJT3dC5Be99iOu/AicFHmY5WkltwZr6GZA4QWKrqx34DJkuI8jsxsHwVEbSjUcVTwYyTtDRBlLWV7ap3yf3Rktq8G8iy0XNKGKgSCYBS4CGH0wjOrVMdhhMf290D0KmE7JYX4rDSVCHV6DrBd0i/DAFfFp+27gdvS2rjEkyTtKR2rVHE+ijW2jwZ+yt61VNJYYwgEcNshuYsiu0nSjU0j8GEUsxSFrZIubBqBV4ALEoFoguY3jcBW4PxE4G1JC5pG4H0gCmzYy5IWNY3Az6EGEoGNkm5uDIEk9UMZF7YkGp0mEbgVuC8rZKHLdjeCgO3A+TUwLRHYJSk6uHIpMYoqW/YO24uBvC9eKWlfNGofgdSdhZCMIUBYNPohp39vCoExYEkWpbWS1hSfax0B23cBt2fg4x7EvPbPWhOwHV3gJuC6DHyMGxdIeje/M7WLgO0zgEeAs9ou902Sosnfz2pDwHbMgeK4RPvYjmu1pHWdslU+mTscOL4spQ3weUiCGcDZwBVxtjv4jlnQ9d2mcq0sZDvCFb8HHDFAgP26+gSIUU78Tt3Viun0k8CV/b5xQPt/ACJNPiQpIjCuFQRuAe4vWzzE51GUQus/DGzO02TZOwsC8TciMLdsw4CeR28bs9bI63FUPqjybY97iQcEbORu/gMh9z5ASOE8MwAAAABJRU5ErkJggg==`;

    return (
        <Stack.Navigator screenOptions={{ headerShown: false, gestureEnabled: false }} initialRouteName={'Splash'}>
            <Stack.Screen name={'Splash'} component={SplashScreen} options={{ headerShown: true, headerBackTitle: '', title: '' }} />
            <Stack.Screen
                name={'Home'}
                component={HomeScreen}
                options={{
                    headerLeft: null,
                    headerShown: true,
                    headerTransparent: true,
                    title: t('home.title'),
                    headerTitleAlign: 'center',
                    headerTitleStyle: styles.headerTitleText,
                    headerStyle: {
                        shadowColor: 'transparent',
                        backgroundColor: '#ffffff08'
                    }
                }}
            />
            <Stack.Screen
                name={'Pack'}
                component={PackScreen}
                options={{
                    headerShown: true,
                    headerTransparent: true,
                    headerMode: 'float',
                    headerTitleAlign: 'center',
                    headerBackTitleVisible: false,
                    title: t('pack.title'),
                    headerTitleStyle: styles.headerTitleText,
                    headerStyle: {
                        shadowColor: 'transparent',
                        backgroundColor: '#394474'
                    },
                    headerBackImage: () => (
                        <View style={styles.backButtonImageContainer}>
                            <Image
                                source={{
                                    uri: imageSource
                                }}
                                style={styles.backButtonImage}
                            />
                        </View>
                    )
                }}
            />
            <Stack.Screen name={'Onboarding'} component={OnboardingScreen} options={{ header: () => null }} />
        </Stack.Navigator>
    );
}

export default function Navigation({ colorScheme }: { colorScheme: ColorSchemeName }) {
    const navigationRef = useNavigationContainerRef();

    return (
        <NavigationContainer ref={navigationRef} theme={CustomTheme}>
            <RootNavigator />
        </NavigationContainer>
    );
}
