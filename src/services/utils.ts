import { NativeModules } from 'react-native';
const { ProdChecker } = NativeModules;

export default class Utils {
    private static envName: string;

    static sendGetEnvName = async (): Promise<string> => {
        return ProdChecker.getEnvName();
    };

    static getEnvName = async () => {
        if (!this.envName) {
            this.envName = await this.sendGetEnvName();
        }

        return this.envName;
    };

    static isDevEnv = async () => {
        return (await this.getEnvName()) === 'DEBUG';
    };
}
