import { Amplitude } from '@amplitude/react-native';
import { Platform } from 'react-native';
import Utils from './utils';

export default class AmplitudeUtils {
    private static AMPLITUDE_API_KEY_DEV = `8f6f1bd726b81bf0d0a25b62d7175348`;

    private static AMPLITUDE_API_KEY_PROD = `a96ed022266f80524602088eeb925063`;

    static initAmplitude = async () => {
        const ampInstance = Amplitude.getInstance();
        let apiKey: string = this.AMPLITUDE_API_KEY_DEV;
        if (Platform.OS === 'ios') {
            apiKey = (await Utils.isDevEnv()) ? this.AMPLITUDE_API_KEY_DEV : this.AMPLITUDE_API_KEY_PROD;
        } else if (Platform.OS === 'android') {
            apiKey = this.AMPLITUDE_API_KEY_PROD;
        }
        ampInstance
            .init(apiKey)
            .then((result) => {
                if (result) {
                    AmplitudeUtils.logEventAmpl('openApp');
                }
            })
            .catch(console.error);
    };

    static setUserID = (userID: string) => {
        Amplitude.getInstance().setUserId(userID).catch(console.error);
    };

    static setUserProperties = (userProperties: Record<string, unknown>) => {
        Amplitude.getInstance().setUserProperties(userProperties).catch(console.error);
    };

    static logEventAmpl = (eventName: string, eventProps?: Record<string, unknown>) => {
        Amplitude.getInstance().logEvent(eventName, eventProps).catch(console.error);
    };
}
