import 'react-native-url-polyfill/auto';
import { DefaultApi, Configuration } from '../generated';
import { aws4Interceptor, Credentials } from 'aws4-axios';
import * as AWS from '@aws-sdk/client-cognito-identity';
import { AxiosError, AxiosResponse } from 'axios';
import { Pack, PackPaths } from '../generated';
import * as Keychain from 'react-native-keychain';
import AmplitudeUtils from 'hypotheticals-pure/src/services/amplitudeUtils';

import { Buffer } from 'buffer';
global.Buffer = global.Buffer || Buffer;

interface CredentialsStoredObject {
    accessKeyId?: string;
    secretAccessKey?: string;
    sessionToken?: string;
    expiration?: Date;
}

export const CHOSEN_LANGUAGE = 'chosenLanguage';
export const LOADED_PACKS_LANGUAGE = 'loadedPacksLanguage';

export default class Api {
    private static BASE_PATH_DEV = 'https://ix6lpckjl9.execute-api.eu-central-1.amazonaws.com/dev';

    private static IDENTITY_POOL_ID = 'eu-central-1:4057ae51-a5a9-4b14-97e4-0d5469128a75';

    private static IDENTITY_ID_KEY = 'identityId';

    private static CREDENETIALS_KEY = 'credentials';

    private static client?: DefaultApi;

    static buildClient = (): DefaultApi => {
        const config = {
            basePath: this.BASE_PATH_DEV
        };

        return new DefaultApi(new Configuration(config));
    };

    static getCredentialsForIdentityPromise = (cognitoidentity: AWS.CognitoIdentity, params: AWS.GetCredentialsForIdentityCommandInput) => {
        return new Promise((resolve, reject) => {
            cognitoidentity.getCredentialsForIdentity(params, function (err, data) {
                if (err) return reject(err);
                resolve(data);
            });
        });
    };

    static getIdPromise = (cognitoidentity: AWS.CognitoIdentity, params: AWS.GetIdCommandInput) => {
        return new Promise((resolve, reject) => {
            cognitoidentity.getId(params, function (err: any, data: any): AWS.GetIdCommandOutput | void {
                if (err) return reject(err);
                resolve(data);
            });
        });
    };

    static saveCredentialsForIdentity = (data: AWS.GetCredentialsForIdentityCommandOutput) => {
        const credentialsUpdate: CredentialsStoredObject = {
            accessKeyId: data.Credentials?.AccessKeyId,
            secretAccessKey: data.Credentials?.SecretKey,
            sessionToken: data.Credentials?.SessionToken,
            expiration: data.Credentials?.Expiration
        };

        Keychain.setInternetCredentials(this.CREDENETIALS_KEY, this.CREDENETIALS_KEY, JSON.stringify(credentialsUpdate)).catch(console.error);
        return credentialsUpdate;
    };

    static getCredentials = async () => {
        const credentials = await this.getKeychainCredentials();
        if (credentials) {
            return credentials;
        }

        const cognitoidentity = new AWS.CognitoIdentity({ region: 'eu-central-1' });

        const paramsIdentityIdKeychain = await Keychain.getInternetCredentials(this.IDENTITY_ID_KEY);

        if (paramsIdentityIdKeychain) {
            //console.log('identityId exist');
            const identityId: string = paramsIdentityIdKeychain.password;
            const paramsIdentityId: AWS.GetCredentialsForIdentityCommandInput = {
                IdentityId: identityId
            };
            return this.getCredentialsForIdentityPromise(cognitoidentity, paramsIdentityId)
                .then((data: AWS.GetCredentialsForIdentityCommandOutput) => {
                    return this.saveCredentialsForIdentity(data);
                })
                .catch((err) => {
                    console.log(`getCredentialsForIdentity error ${err}`);
                });
        } else {
            const params = {
                IdentityPoolId: this.IDENTITY_POOL_ID
            };
            return this.getIdPromise(cognitoidentity, params)
                .then((data: AWS.GetIdCommandOutput) => {
                    const paramsIdentityId = {
                        IdentityId: data.IdentityId
                    };
                    Keychain.setInternetCredentials(this.IDENTITY_ID_KEY, this.IDENTITY_ID_KEY, data.IdentityId).catch(console.error);
                    AmplitudeUtils.setUserProperties({ identityId: data.IdentityId });
                    return this.getCredentialsForIdentityPromise(cognitoidentity, paramsIdentityId);
                })
                .then((data: AWS.GetCredentialsForIdentityCommandOutput) => {
                    return this.saveCredentialsForIdentity(data);
                })
                .catch((err) => {
                    console.log(`getCredentialsForIdentity error ${err}`);
                });
        }
    };

    static getKeychainCredentials = async () => {
        const credentials = await Keychain.getInternetCredentials(this.CREDENETIALS_KEY);
        if (credentials) {
            const credentialsObject: CredentialsStoredObject = JSON.parse(credentials.password);
            if (credentialsObject.expiration && Date.parse(credentialsObject.expiration) > new Date().getTime()) {
                //console.log('creds exist');
                return credentialsObject;
            }
        }
        return false;
    };

    static getApiClient = async (): Promise<DefaultApi> => {
        if (!this.client || !(await this.getKeychainCredentials())) {
            const defaultApi = this.buildClient();

            const credentials = await this.getCredentials();

            const interceptor = aws4Interceptor({}, credentials);

            defaultApi.axios.interceptors.request.use(interceptor);

            this.client = defaultApi;
        }

        return this.client;
    };

    static clearApiClient = (): void => {
        this.client = undefined;
    };

    static defaultCatchHandler = (err: Error | AxiosError): Promise<AxiosResponse<Pack[], any>> => {
        const axiosError = err as AxiosError;

        Promise.reject(err);
    };

    static getPacks = async (): Promise<AxiosResponse<Pack[], unknown>> => {
        const defaultApi = await this.getApiClient();

        return defaultApi?.packsGet().catch(this.defaultCatchHandler);
    };

    static getPacksPaths = async (packs: string[], lang: string): Promise<AxiosResponse<PackPaths[], unknown>> => {
        const defaultApi = await this.getApiClient();

        return defaultApi?.packspathsGet(packs, lang).catch(this.defaultCatchHandler);
    };

    static answersPost = async (dilemmaId: string, answer: boolean): Promise<AxiosResponse<Pack[], any> | AxiosResponse<void, any>> => {
        const defaultApi = await this.getApiClient();

        return defaultApi?.answersPost({ dilemma_id: dilemmaId, answer: answer.toString() }).catch(this.defaultCatchHandler);
    };

    static getPackData = async (query: string): Promise<any> => {
        const url = `${query}`;
        const response = await fetch(url);
        return response.json();
    };

    static contentReportsPost = async (dilemmaId: string): Promise<AxiosResponse<Pack[], any> | AxiosResponse<void, any>> => {
        const defaultApi = await this.getApiClient();

        return defaultApi?.contentReportsPost({ dilemma_id: dilemmaId, notes: '' }).catch(this.defaultCatchHandler);
    };
}
