/**
 * Learn more about using TypeScript with React Navigation:
 * https://reactnavigation.org/docs/typescript/
 */
import { TextInput as DefaultTextInput, ActivityIndicator as Loading, TouchableOpacity } from 'react-native';
import { StackScreenProps } from '@react-navigation/stack';

export type RootStackParamList = {
    Splash: undefined;
    Home: undefined;
    Pack: undefined;
    Onboarding: undefined;
};

export type PackTabParamList = {
    Pack: { packId: string };
};

export type HomeTabParamList = {
    Home: undefined;
    Pack: { packId: string };
};

export type SplashTabParamList = {
    Splash: undefined;
    Onboarding: undefined;
    Home: undefined;
};

export type OnboardingTabParamList = {
    Onboarding: undefined;
    Home: undefined;
};

export type ButtonProps = TouchableOpacity['props'] & {
    onPress: () => void;
    text: string;
};

export type ThemeProps = {
    lightColor?: string;
    darkColor?: string;
    text?: string;
};

export type InputProps = ThemeProps &
    DefaultTextInput['props'] & {
        isValid?: boolean;
        isLeftIcon?: boolean;
        wrapperStyle?: any;
        showClearButton?: boolean;
        clearButtonPressed?: () => void;
        autoCompleteType?: string;
        leftIcon?: string;
        leftIconColor?: string;
        leftIconStyle?: object;
    };

export type MenuProps = ThemeProps & { signOutHandler: () => void };

export type LoadingProps = Loading['props'] & { isVisible?: boolean };

export type SignInScreenProps = StackScreenProps<AuthStackParamList, 'SignIn'> & { googleSignIn: () => void } & { appleSignIn: () => void };

export type IsMyWordIconProps = { hasUserWord: boolean; onPressCallback: () => void; size: number; color?: string };

export type LanguageSettingsType = {
    locale: string;
    name: string;
    svg_image: string;
};
