import React, { useMemo } from 'react';
import { StatusBar, LogBox } from 'react-native';

import { SafeAreaProvider } from 'react-native-safe-area-context';
import { RootSiblingParent } from 'react-native-root-siblings';

import useColorScheme from 'hypotheticals-pure/src/hooks/useColorScheme';
import Navigation from 'hypotheticals-pure/src/navigation';
import AmplitudeUtils from 'hypotheticals-pure/src/services/amplitudeUtils';
import { withIAPContext } from 'react-native-iap';

import { TourGuideProvider } from 'rn-tourguide';

//import { NativeModules } from 'react-native';
//NativeModules.DevSettings.setIsDebuggingRemotely(false);

import 'react-native-get-random-values';

const CustomStatusBar = () => <StatusBar translucent={true} barStyle={'light-content'} />;

void AmplitudeUtils.initAmplitude();

LogBox.ignoreLogs([`[react-native-gesture-handler] Seems like you're using an old API with gesture components, check out new Gestures system!`]);

const App = () => {
    const colorScheme = useColorScheme();

    const localLabels = useMemo(
        () => ({
            labels: {
                previous: 'Назад',
                next: 'Далее',
                skip: 'Пропустить',
                finish: 'Закончить'
            }
        }),
        []
    );

    return (
        <SafeAreaProvider>
            <RootSiblingParent>
                <TourGuideProvider {...localLabels} preventOutsideInteraction={true}>
                    <Navigation colorScheme={colorScheme} />
                    <CustomStatusBar />
                </TourGuideProvider>
            </RootSiblingParent>
        </SafeAreaProvider>
    );
};

export default withIAPContext(App);
