#import "ProdChecker.h"

@implementation ProdChecker

RCT_EXPORT_MODULE();

RCT_REMAP_METHOD(getEnvName, resolver: (RCTPromiseResolveBlock)resolve
                 rejecter:(RCTPromiseRejectBlock)reject)
{
  #ifdef DEBUG
      BOOL const DEBUG_BUILD = YES;
  #else
      BOOL const DEBUG_BUILD = NO;
  #endif
  NSURL *receiptURL = [[NSBundle mainBundle] appStoreReceiptURL];
  NSString *receiptURLString = [receiptURL path];
  BOOL isRunningTestFlightBeta =  ([receiptURLString rangeOfString:@"sandboxReceipt"].location != NSNotFound);
  if (DEBUG_BUILD) {
    NSString *thingToReturn = @"DEBUG";
    resolve(thingToReturn);
  } else if(isRunningTestFlightBeta) {
    NSString *thingToReturn = @"TESTFLIGHT";
    resolve(thingToReturn);
  } else {
    NSString *thingToReturn = @"PRODUCTION";
    resolve(thingToReturn);
  }
}

@end
